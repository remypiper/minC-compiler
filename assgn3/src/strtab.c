#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<strtab.h>

/* Provided is a hash function that you may call to get an integer back. */
unsigned long hash(unsigned char *str) {
    unsigned long hash = 5381;
    int c;

    while (c = *str++)
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash;
}

int ST_insert(char *id, char *scope, int data_type, int symbol_type, int size, param* params) {
    // Allocate memory for a new entry, and put data into that object
    struct strEntry *newEntry = (struct strEntry*) malloc(sizeof(struct strEntry));
    newEntry->id = id;
    newEntry->scope = scope;
    newEntry->data_type = data_type;
    newEntry->symbol_type = symbol_type;
    newEntry->size = size;
    newEntry->params = params;

    // Concatenate the scope and id and use that to create the hash key
    char str1[50];
    char str2[50];
    strcpy(str1, id);
    strcpy(str2, scope);
    char* key = strncat(str1, str2, 100);
    int index = hash(key) % 1000;
    
    //printf("in insert 1\n");

    //printf("in insert 2\n");
	for (int i = 0; i < 1000; i++){
		index = (index + i) % 1000;
    		if (strTable[index]->id == NULL){
    			//printf("in insert 4\n");
       			strTable[index] = newEntry;
       			return index; 
    		}	

    		else if (strTable[index]->id != NULL && !strcmp(strTable[index]->id, "")){
    			//printf("in insert 5\n");
       			strTable[index] = newEntry;
       			return index; 
    		}	
		else if (strTable[index]->id != NULL && !strcmp(strTable[index]->id, id) && !strcmp(strTable[index]->scope, scope)){
    			//printf("in insert 6\n");
			//printf("Error: : Symbol declared multiple times: %s\n", id);
       			return index; 
		}
       	}
	//printf("Error: : Symbol declared multiple times: %s\n", id);
	
    return index;    
}

int ST_lookup(char *id, char *scope) {
 /*   
    // Concatenate the scope and id and use that to create the hash key
    char str1[50];
    char str2[50];
    strcpy(str1, id);
    strcpy(str2, scope);
    char* key = strncat(str1, str2, 100);
    int index = hash(key) % 1000;
    
    // TODO: Use the hash value to check if the index position has the "id". If not, keep looking for id until you find an empty spot. If you find "id", return that index. If you arrive at an empty spot, that means "id" is not there. Then return -1. 
    while (strTable[index]->id != NULL) {
        if (strTable[index]->id == id) {
            return index;
        }
        else { index++; } // If it isn't there, then it's either a few spots ahead, or not in the table. If we reach null before finding it, then it isn't in the table.
    }
    return -1; // Only reached if we find an empty space somewhere in front of the initial index.
*/
// Concatenate the scope and id and use that to create the hash key
   
    for (int i = 0; i < 1000; i++) {
        if (strTable[i]->id != NULL && !strcmp(strTable[i]->id,id)) {
	    //printf("in if\n");
            return i;
        }
    }
    
    return -1; // Only reached if we find an empty space somewhere in front of the initial index.

}

char* ST_getId(int index) { // Returns the value given a key
     return strTable[index]->id;
}

char *dataType[9] = {"INT_TYPE", "CHAR_TYPE", "VOID_TYPE", "INT_ARR", "CHAR_ARR", "VOID_ARR", "INT_FUNC", "CHAR_FUNC", "VOID_FUNC"};
char *symbolType[3] =  {"SCALAR", "ARRAY", "FUNCTION"};

void print_sym_tab(){
        printf("\n\nSYMBOL TABLE:\n");
        for(int i = 0; i < MAXIDS; i++){
            if(strTable[i]->id != NULL){
                output_entry(i);
            }
        }
}

void output_entry(int i) {
    printf("%d: %s ", i, dataType[strTable[i]->data_type]);
    printf("%s: %s %s \n", strTable[i]->scope, strTable[i]->id, symbolType[strTable[i]->symbol_type]);
}
