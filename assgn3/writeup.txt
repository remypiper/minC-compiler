1. My name is Remy Piper, (CS-4318) and my partner's name is Garrett Faber (CS-5331)

2. We have been using git and Discord to work on the project. We plan to work as often as we can on assignment 4.

3. Virtually all of this assignment was done by Garrett. I did a little bit to try and fix the scope detection, but it does not work. All
other improvements were done by Garrett.

4. Garrett made a lot more test cases in the test1 directory.
