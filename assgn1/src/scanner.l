/* definitions */

%{
#include<stdio.h>
#include "tokendef.h"

int scancol = 1;
int yycol = 1;
int scanlineno = 1;

char* yyerror;

void updateLine();
void updateCol();
void countLines();
int processString();
%}

newline         \n
whitespace      [\t ]+
integer         0|[1-9][0-9]*
integerZero 	0[0-9]*
character       \'([^\']|\\[nt0])\'

/* String can contain any characters between the double quotes other than a newline or
// unescaped doublequotes.
*/

string	    	\"([^\"\n]|(\\\"))*[^\\\n]\"

/* multiline string has \n somewhere in the middle */

multlnstring   {untermstring}{newline}([^\"]|\\\")*\"

/* If the end quote is found, one of the two regexes above will be matched. Otherwise,
// the string is unterminated.
*/

untermstring    \"([^\"\n]|(\\\"))*

comment         \/\/[^\n]*
multlncomment   \/\*([^\*]|(\*)*[^\*\/])*\*(\*)*\/
untermcomment   \/\*([^\*\n]|\*[^\/\n])*[^\/\n]|\/\*
identifier      [a-zA-Z][a-zA-Z0-9]*
illidentifier   [0-9]+[a-zA-Z][a-zA-Z0-9]*

yylineno = 1;

%%

 /* rules  */

 /* Keywords */;
"if" 		{updateCol(); return KWD_IF;}
"else"		{updateCol(); return KWD_ELSE;}
"while"		{updateCol(); return KWD_WHILE;}
"int"		{updateCol(); return KWD_INT;}
"string"	{updateCol(); return KWD_STRING;}
"char"		{updateCol(); return KWD_CHAR;}
"return"	{updateCol(); return KWD_RETURN;}
"void"		{updateCol(); return KWD_VOID;}


 /* operators */;

\+      {updateCol(); return OPER_ADD;}
\-      {updateCol(); return OPER_SUB;}
\*      {updateCol(); return OPER_MUL;}
\/      {updateCol(); return OPER_DIV;}
\<      {updateCol(); return OPER_LT;}
\>      {updateCol(); return OPER_GT;} 
\>\=    {updateCol(); return OPER_GTE;}
\<\=    {updateCol(); return OPER_LTE;}
\=\=    {updateCol(); return OPER_EQ;}
\!\=    {updateCol(); return OPER_NEQ;}
\=      {updateCol(); return OPER_ASGN;}
\+\+    {updateCol(); return OPER_INC;}
\-\-    {updateCol(); return OPER_DEC;}
\&\&    {updateCol(); return OPER_AND;}
\|\|    {updateCol(); return OPER_OR;}
\!      {updateCol(); return OPER_NOT;}
\@      {updateCol(); return OPER_AT;}
\%      {updateCol(); return OPER_MOD;}


 /* brackets and parens */;
\[      {updateCol(); return LSQ_BRKT;}
\]      {updateCol(); return RSQ_BRKT;}
\{      {updateCol(); return LCRLY_BRKT;}
\}      {updateCol(); return RCRLY_BRKT;}
\(      {updateCol(); return LPAREN;}
\)      {updateCol(); return RPAREN;}

 /* puncuation */
,   	{updateCol(); return COMMA;}
;       {updateCol(); return SEMICLN;}


 /* Identifiers */;
{identifier}    {updateCol(); return ID;}
{illidentifier} {updateCol(); yyerror = "Identifier may not begin with an integer"; return ERROR;}

 /* Constants */;
{integer}       {updateCol(); return INTCONST;}
{integerZero}	{updateCol(); yyerror = "Integer may not begin with a zero"; return ERROR;}
{character}     {updateCol(); return CHARCONST;}
{string}        {updateCol(); return processString();}
{untermstring}  {updateCol(); yyerror = "Unterminated string"; return ERROR;}
{multlnstring}  {updateCol(); countLines(); yyerror = "String spans multiple lines"; return ERROR;} // Catching a multi-line string and generating an error.

 /* Comments */;
{comment}       {updateCol(); /* do nothing */}
{untermcomment} {updateCol(); yyerror = "Unterminated comment"; return ERROR;}
{multlncomment} {updateCol(); countLines(); /* do nothing*/}

 /* Other */;
{newline}       {updateLine(); return NEWLINE;}
{whitespace}    {updateCol(); return WHITESPACE;}
.               {return ILLEGAL_TOK;}

%%

/* user routines */

void updateCol(){
    yycol = scancol;
	scancol += yyleng;
}

// This function is called when a newline is encountered
// Update the scancol variable to 1
// Increment the scanlinenoo variable
void updateLine(){
	scancol = 1;
	scanlineno++;
	yylineno = scanlineno;
}

// Count the number of newline characters encountered in multiline strings and multiline comments
// Increment the scanlineno variable accordingly
void countLines(){
	yylineno = scanlineno;
	for(int i = 0; i < yyleng; i++)
		if(yytext[i] == '\n')
			scanlineno++;
}


int processString(){
	for(int i = 0; i < yyleng - 1; i++){
		if(yytext[i] == '\\'){
			if(yytext[i+1] == 'n'){
				yytext[i] = '\n';
				yytext[i+1] = ' ';
			}
			else if(yytext[i+1] == 't'){
				yytext[i] = '\t';
				yytext[i+1] = ' ';
			}
			else if(yytext[i+1] == '\\'){
				yytext[i] = '\\';
				yytext[i+1] = ' ';
			}
			else if(yytext[i+1] == '\"'){
				yytext[i] = '\"';
				yytext[i+1] = ' ';
			}
			else{ // It isn't \n \t \\ or \"
				yyerror = "Unrecognized escape character in String";
                yycol = i + 2; // Set the column where the bad escape char is
				return ERROR;
			} //end else
		} //end big if
    } // end for
	return STRCONST;
}












