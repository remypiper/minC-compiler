A compiler written in YACC and C for a minimal variant of the C programming language called minC. Written by Remy Piper and Garrett Faber.
