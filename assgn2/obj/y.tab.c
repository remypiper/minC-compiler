#ifndef lint
static const char yysccsid[] = "@(#)yaccpar	1.9 (Berkeley) 02/21/93";
#endif

#define YYBYACC 1
#define YYMAJOR 1
#define YYMINOR 9
#define YYPATCH 20130304

#define YYEMPTY        (-1)
#define yyclearin      (yychar = YYEMPTY)
#define yyerrok        (yyerrflag = 0)
#define YYRECOVERING() (yyerrflag != 0)

#define YYPREFIX "yy"

#define YYPURE 0

#line 2 "src/parser.y"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<../src/tree.h>
#include<../src/strtab.h>

extern int yylineno;

/* NOTE: mC has two kinds of scopes for variables : local and global. Variables declared outside any
function are considered globals, whereas variables (and parameters) declared inside a function foo are local to foo. You should update the
scope variable whenever you are inside a production that matches function definition (funDecl production). The rationale is that you are
entering that function, so all variables, arrays, and other functions should be within this scope. You should pass this variable whenever
you are calling the ST_insert or ST_lookup functions. This variable should be updated to scope = "" to indicate global scope whenever funDecl finishes. Treat these hints as helpful directions only. You may implement all of the functions as you like and not adhere to my instructions. As long as the directory structure is correct and the file names are correct, we are okay with it. */
char* scope = "";
#line 19 "src/parser.y"
#ifdef YYSTYPE
#undef  YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
#endif
#ifndef YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
typedef union
{
    int value;
    struct treenode *node;
    char *strval;
} YYSTYPE;
#endif /* !YYSTYPE_IS_DECLARED */
#line 48 "obj/y.tab.c"

/* compatibility with bison */
#ifdef YYPARSE_PARAM
/* compatibility with FreeBSD */
# ifdef YYPARSE_PARAM_TYPE
#  define YYPARSE_DECL() yyparse(YYPARSE_PARAM_TYPE YYPARSE_PARAM)
# else
#  define YYPARSE_DECL() yyparse(void *YYPARSE_PARAM)
# endif
#else
# define YYPARSE_DECL() yyparse(void)
#endif

/* Parameters sent to lex. */
#ifdef YYLEX_PARAM
# define YYLEX_DECL() yylex(void *YYLEX_PARAM)
# define YYLEX yylex(YYLEX_PARAM)
#else
# define YYLEX_DECL() yylex(void)
# define YYLEX yylex()
#endif

/* Parameters sent to yyerror. */
#ifndef YYERROR_DECL
#define YYERROR_DECL() yyerror(const char *s)
#endif
#ifndef YYERROR_CALL
#define YYERROR_CALL(msg) yyerror(msg)
#endif

extern int YYPARSE_DECL();

#define ID 257
#define INTCONST 258
#define CHARCONST 259
#define STRCONST 260
#define KWD_IF 261
#define KWD_ELSE 262
#define KWD_WHILE 263
#define KWD_INT 264
#define KWD_STRING 265
#define KWD_CHAR 266
#define KWD_RETURN 267
#define KWD_VOID 268
#define OPER_ADD 269
#define OPER_SUB 270
#define OPER_MUL 271
#define OPER_DIV 272
#define OPER_LT 273
#define OPER_GT 274
#define OPER_GTE 275
#define OPER_LTE 276
#define OPER_EQ 277
#define OPER_NEQ 278
#define OPER_ASGN 279
#define OPER_INC 280
#define OPER_DEC 281
#define OPER_AND 282
#define OPER_OR 283
#define OPER_NOT 284
#define OPER_AT 285
#define OPER_MOD 286
#define WHITESPACE 287
#define NEWLINE 288
#define LSQ_BRKT 289
#define RSQ_BRKT 290
#define LCRLY_BRKT 291
#define RCRLY_BRKT 292
#define LPAREN 293
#define RPAREN 294
#define COMMA 295
#define SEMICLN 296
#define ILLEGAL_TOKEN 297
#define ERROR 298
#define YYERRCODE 256
static const short yylhs[] = {                           -1,
    0,    1,    1,    3,    3,    4,    4,    2,    2,    2,
    5,    5,    5,    5,    6,    6,    7,    7,    9,    9,
   11,   11,   11,   11,   11,   10,   10,    8,    8,    8,
   12,   13,   13,   14,   14,   15,   16,   16,   17,   17,
   18,   18,   19,   19,   19,   19,   19,   19,   20,   20,
   21,   21,   22,   22,   23,   23,   24,   24,   24,   24,
   24,   24,   25,   25,   26,   26,
};
static const short yylen[] = {                            2,
    1,    1,    2,    1,    1,    6,    3,    1,    1,    1,
    7,    6,    5,    6,    1,    3,    2,    4,    1,    2,
    1,    1,    1,    1,    1,    1,    2,    3,    3,    4,
    3,    4,    2,    5,    7,    5,    2,    3,    1,    4,
    1,    3,    1,    1,    1,    1,    1,    1,    1,    3,
    1,    1,    1,    3,    1,    1,    3,    1,    1,    1,
    1,    1,    4,    3,    1,    3,
};
static const short yydefred[] = {                         0,
    8,    9,   10,    0,    0,    0,    2,    4,    5,    3,
    0,    0,    0,    7,    0,    0,    0,    0,    0,    0,
    0,   13,    0,    0,    0,    6,    0,   60,   61,   62,
    0,    0,    0,    0,   14,    0,    0,    0,    0,    0,
    0,   21,   22,   23,   24,   25,    0,    0,    0,    0,
   53,   59,    0,    0,   12,   16,    0,    0,    0,    0,
   37,   58,    0,    0,    0,    0,   20,   29,    0,   28,
   27,    0,   44,   45,   46,   43,   47,   48,   33,    0,
   51,   52,    0,   55,   56,    0,   18,   11,    0,   64,
    0,    0,    0,    0,   38,   31,   57,   30,    0,    0,
    0,   54,   40,   63,    0,    0,    0,   32,    0,    0,
   36,    0,   35,
};
static const short yydgoto[] = {                          4,
    5,   37,    7,   38,    9,   18,   19,   22,   39,   40,
   41,   42,   43,   44,   45,   46,   62,   48,   80,   49,
   83,   50,   86,   51,   52,   92,
};
static const short yysindex[] = {                      -163,
    0,    0,    0,    0, -163, -254,    0,    0,    0,    0,
 -105, -243, -176,    0, -256, -236, -193, -227, -223, -217,
 -210,    0, -202, -187, -163,    0, -250,    0,    0,    0,
 -204, -186, -252, -134,    0, -222, -141, -163, -183, -170,
 -134,    0,    0,    0,    0,    0, -151, -138, -150,  -89,
    0,    0, -156, -198,    0,    0, -222, -248, -222, -222,
    0,    0, -123, -148,  -12, -273,    0,    0, -143,    0,
    0, -222,    0,    0,    0,    0,    0,    0,    0, -222,
    0,    0, -222,    0,    0, -222,    0,    0, -238,    0,
   16, -101,   -6,    2,    0,    0,    0,    0,  -22, -150,
  -89,    0,    0,    0, -222, -134, -134,    0,   16, -121,
    0, -134,    0,
};
static const short yyrindex[] = {                         0,
    0,    0,    0,    0,  156,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0, -120,    0,
    0,    0,  -93,    0,    0,    0, -109,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0, -161,    0,    0,
 -103,    0,    0,    0,    0,    0,  -60,    0,  -46,  -98,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
  -73,    0,    0,    0,    0,    0,    0,    0,    0,  -36,
  -70,    0,    0,    0,    0,    0,    0,    0,  -61, -146,
    0,    0,    0,
};
static const short yygindex[] = {                         0,
    0,   17,  185,   24,    0,  170,    0,  195,  197,  -20,
   36,    0,    0,    0,    0,    0,  -21,  -32,    0,  -55,
    0,  140,    0,  157,    0,    0,
};
#define YYTABLESIZE 296
static const short yytable[] = {                         47,
   63,   89,   11,   65,   27,   28,   29,   30,   27,   28,
   29,   30,   47,   64,   15,   12,    6,   47,   69,   47,
   71,    6,   14,    8,  100,   91,   93,   94,    8,   17,
   81,   82,   47,   20,   27,   28,   29,   30,   57,   99,
   36,   17,   58,   61,   36,   90,   27,   28,   29,   30,
   31,  103,   32,    1,   21,    2,   33,    3,   27,   28,
   29,   30,   31,   23,   32,    1,   24,    2,   33,    3,
   36,   25,  109,   27,   28,   29,   30,   31,   26,   32,
   34,   35,   36,   33,   47,   47,   53,    1,   59,    2,
   47,    3,   34,   88,   36,   19,   19,   19,   19,   19,
    1,   19,    2,   54,    3,   19,   60,   34,   68,   36,
   34,   34,   34,   34,   34,   66,   34,   16,   81,   82,
   34,   70,   27,   28,   29,   30,   31,   72,   32,   19,
   19,   19,   33,   87,   73,   74,   75,   76,   77,   78,
  112,  110,  111,   96,   34,   34,   34,  113,   98,   73,
   74,   75,   76,   77,   78,    1,   34,   79,   36,   39,
   39,   39,   39,   39,   39,   39,   39,   39,   39,   39,
   49,   49,   95,   15,   49,   49,   49,   49,   49,   49,
   39,   84,   85,   12,   39,   39,   39,   13,   26,   10,
   14,   49,  104,  105,   56,   49,   49,   49,   50,   50,
   17,   17,   50,   50,   50,   50,   50,   50,   58,   58,
   58,   58,   58,   58,   58,   58,   58,   58,   55,   50,
   65,   65,  101,   50,   50,   50,   41,   41,   41,   41,
   41,   41,   66,   66,   67,   58,   42,   42,   42,   42,
   42,   42,  102,    0,    0,    0,    0,   41,   41,   41,
   73,   74,   75,   76,   77,   78,    0,   42,   42,   42,
   73,   74,   75,   76,   77,   78,   73,   74,   75,   76,
   77,   78,    0,  108,   73,   74,   75,   76,   77,   78,
    0,   97,    0,    0,    0,    0,    0,  106,   73,   74,
   75,   76,   77,   78,    0,  107,
};
static const short yycheck[] = {                         21,
   33,   57,  257,   36,  257,  258,  259,  260,  257,  258,
  259,  260,   34,   34,  258,  289,    0,   39,   39,   41,
   41,    5,  296,    0,   80,   58,   59,   60,    5,   13,
  269,  270,   54,  290,  257,  258,  259,  260,  289,   72,
  293,   25,  293,  296,  293,  294,  257,  258,  259,  260,
  261,  290,  263,  264,  291,  266,  267,  268,  257,  258,
  259,  260,  261,  257,  263,  264,  294,  266,  267,  268,
  293,  295,  105,  257,  258,  259,  260,  261,  296,  263,
  291,  292,  293,  267,  106,  107,  289,  264,  293,  266,
  112,  268,  291,  292,  293,  257,  258,  259,  260,  261,
  264,  263,  266,  291,  268,  267,  293,  291,  292,  293,
  257,  258,  259,  260,  261,  257,  263,  294,  269,  270,
  267,  292,  257,  258,  259,  260,  261,  279,  263,  291,
  292,  293,  267,  290,  273,  274,  275,  276,  277,  278,
  262,  106,  107,  292,  291,  292,  293,  112,  292,  273,
  274,  275,  276,  277,  278,    0,  291,  296,  293,  269,
  270,  271,  272,  273,  274,  275,  276,  277,  278,  279,
  269,  270,  296,  294,  273,  274,  275,  276,  277,  278,
  290,  271,  272,  289,  294,  295,  296,  293,  292,    5,
  296,  290,  294,  295,   25,  294,  295,  296,  269,  270,
  294,  295,  273,  274,  275,  276,  277,  278,  269,  270,
  271,  272,  273,  274,  275,  276,  277,  278,   24,  290,
  294,  295,   83,  294,  295,  296,  273,  274,  275,  276,
  277,  278,  294,  295,   38,  296,  273,  274,  275,  276,
  277,  278,   86,   -1,   -1,   -1,   -1,  294,  295,  296,
  273,  274,  275,  276,  277,  278,   -1,  294,  295,  296,
  273,  274,  275,  276,  277,  278,  273,  274,  275,  276,
  277,  278,   -1,  296,  273,  274,  275,  276,  277,  278,
   -1,  294,   -1,   -1,   -1,   -1,   -1,  294,  273,  274,
  275,  276,  277,  278,   -1,  294,
};
#define YYFINAL 4
#ifndef YYDEBUG
#define YYDEBUG 0
#endif
#define YYMAXTOKEN 298
#if YYDEBUG
static const char *yyname[] = {

"end-of-file",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"ID","INTCONST","CHARCONST",
"STRCONST","KWD_IF","KWD_ELSE","KWD_WHILE","KWD_INT","KWD_STRING","KWD_CHAR",
"KWD_RETURN","KWD_VOID","OPER_ADD","OPER_SUB","OPER_MUL","OPER_DIV","OPER_LT",
"OPER_GT","OPER_GTE","OPER_LTE","OPER_EQ","OPER_NEQ","OPER_ASGN","OPER_INC",
"OPER_DEC","OPER_AND","OPER_OR","OPER_NOT","OPER_AT","OPER_MOD","WHITESPACE",
"NEWLINE","LSQ_BRKT","RSQ_BRKT","LCRLY_BRKT","RCRLY_BRKT","LPAREN","RPAREN",
"COMMA","SEMICLN","ILLEGAL_TOKEN","ERROR",
};
static const char *yyrule[] = {
"$accept : program",
"program : declList",
"declList : decl",
"declList : declList decl",
"decl : varDecl",
"decl : funDecl",
"varDecl : typeSpecifier ID LSQ_BRKT INTCONST RSQ_BRKT SEMICLN",
"varDecl : typeSpecifier ID SEMICLN",
"typeSpecifier : KWD_INT",
"typeSpecifier : KWD_CHAR",
"typeSpecifier : KWD_VOID",
"funDecl : typeSpecifier ID LPAREN formalDeclList RPAREN LCRLY_BRKT RCRLY_BRKT",
"funDecl : typeSpecifier ID LPAREN formalDeclList RPAREN funBody",
"funDecl : typeSpecifier ID LPAREN RPAREN funBody",
"funDecl : typeSpecifier ID LPAREN RPAREN LCRLY_BRKT RCRLY_BRKT",
"formalDeclList : formalDecl",
"formalDeclList : formalDecl COMMA formalDeclList",
"formalDecl : typeSpecifier ID",
"formalDecl : typeSpecifier ID LSQ_BRKT RSQ_BRKT",
"localDeclList : varDecl",
"localDeclList : varDecl localDeclList",
"statement : compoundStmt",
"statement : assignStmt",
"statement : condStmt",
"statement : loopStmt",
"statement : returnStmt",
"statementList : statement",
"statementList : statement statementList",
"funBody : LCRLY_BRKT statementList RCRLY_BRKT",
"funBody : LCRLY_BRKT localDeclList RCRLY_BRKT",
"funBody : LCRLY_BRKT localDeclList statementList RCRLY_BRKT",
"compoundStmt : LCRLY_BRKT statementList RCRLY_BRKT",
"assignStmt : var OPER_ASGN expression SEMICLN",
"assignStmt : expression SEMICLN",
"condStmt : KWD_IF LPAREN expression RPAREN statement",
"condStmt : KWD_IF LPAREN expression RPAREN statement KWD_ELSE statement",
"loopStmt : KWD_WHILE LPAREN expression RPAREN statement",
"returnStmt : KWD_RETURN SEMICLN",
"returnStmt : KWD_RETURN expression SEMICLN",
"var : ID",
"var : ID LSQ_BRKT addExpr RSQ_BRKT",
"expression : addExpr",
"expression : expression relop addExpr",
"relop : OPER_LTE",
"relop : OPER_LT",
"relop : OPER_GT",
"relop : OPER_GTE",
"relop : OPER_EQ",
"relop : OPER_NEQ",
"addExpr : term",
"addExpr : addExpr addop term",
"addop : OPER_ADD",
"addop : OPER_SUB",
"term : factor",
"term : term mulop factor",
"mulop : OPER_MUL",
"mulop : OPER_DIV",
"factor : LPAREN expression RPAREN",
"factor : var",
"factor : funcCallExpr",
"factor : INTCONST",
"factor : CHARCONST",
"factor : STRCONST",
"funcCallExpr : ID LPAREN argList RPAREN",
"funcCallExpr : ID LPAREN RPAREN",
"argList : expression",
"argList : argList COMMA expression",

};
#endif

int      yydebug;
int      yynerrs;

int      yyerrflag;
int      yychar;
YYSTYPE  yyval;
YYSTYPE  yylval;

/* define the initial stack-sizes */
#ifdef YYSTACKSIZE
#undef YYMAXDEPTH
#define YYMAXDEPTH  YYSTACKSIZE
#else
#ifdef YYMAXDEPTH
#define YYSTACKSIZE YYMAXDEPTH
#else
#define YYSTACKSIZE 10000
#define YYMAXDEPTH  500
#endif
#endif

#define YYINITSTACKSIZE 500

typedef struct {
    unsigned stacksize;
    short    *s_base;
    short    *s_mark;
    short    *s_last;
    YYSTYPE  *l_base;
    YYSTYPE  *l_mark;
} YYSTACKDATA;
/* variables for the parser stack */
static YYSTACKDATA yystack;
#line 573 "src/parser.y"

int yywarning(char * msg){
    printf("warning: line %d: %s\n", yylineno, msg);
   return 0;
}

int yyerror(char * msg){
    printf("error: line %d: %s\n", yylineno, msg);
    return 0;
}
#line 397 "obj/y.tab.c"

#if YYDEBUG
#include <stdio.h>		/* needed for printf */
#endif

#include <stdlib.h>	/* needed for malloc, etc */
#include <string.h>	/* needed for memset */

/* allocate initial stack or double stack size, up to YYMAXDEPTH */
static int yygrowstack(YYSTACKDATA *data)
{
    int i;
    unsigned newsize;
    short *newss;
    YYSTYPE *newvs;

    if ((newsize = data->stacksize) == 0)
        newsize = YYINITSTACKSIZE;
    else if (newsize >= YYMAXDEPTH)
        return -1;
    else if ((newsize *= 2) > YYMAXDEPTH)
        newsize = YYMAXDEPTH;

    i = (int) (data->s_mark - data->s_base);
    newss = (short *)realloc(data->s_base, newsize * sizeof(*newss));
    if (newss == 0)
        return -1;

    data->s_base = newss;
    data->s_mark = newss + i;

    newvs = (YYSTYPE *)realloc(data->l_base, newsize * sizeof(*newvs));
    if (newvs == 0)
        return -1;

    data->l_base = newvs;
    data->l_mark = newvs + i;

    data->stacksize = newsize;
    data->s_last = data->s_base + newsize - 1;
    return 0;
}

#if YYPURE || defined(YY_NO_LEAKS)
static void yyfreestack(YYSTACKDATA *data)
{
    free(data->s_base);
    free(data->l_base);
    memset(data, 0, sizeof(*data));
}
#else
#define yyfreestack(data) /* nothing */
#endif

#define YYABORT  goto yyabort
#define YYREJECT goto yyabort
#define YYACCEPT goto yyaccept
#define YYERROR  goto yyerrlab

int
YYPARSE_DECL()
{
    int yym, yyn, yystate;
#if YYDEBUG
    const char *yys;

    if ((yys = getenv("YYDEBUG")) != 0)
    {
        yyn = *yys;
        if (yyn >= '0' && yyn <= '9')
            yydebug = yyn - '0';
    }
#endif

    yynerrs = 0;
    yyerrflag = 0;
    yychar = YYEMPTY;
    yystate = 0;

#if YYPURE
    memset(&yystack, 0, sizeof(yystack));
#endif

    if (yystack.s_base == NULL && yygrowstack(&yystack)) goto yyoverflow;
    yystack.s_mark = yystack.s_base;
    yystack.l_mark = yystack.l_base;
    yystate = 0;
    *yystack.s_mark = 0;

yyloop:
    if ((yyn = yydefred[yystate]) != 0) goto yyreduce;
    if (yychar < 0)
    {
        if ((yychar = YYLEX) < 0) yychar = 0;
#if YYDEBUG
        if (yydebug)
        {
            yys = 0;
            if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
            if (!yys) yys = "illegal-symbol";
            printf("%sdebug: state %d, reading %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
    }
    if ((yyn = yysindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: state %d, shifting to state %d\n",
                    YYPREFIX, yystate, yytable[yyn]);
#endif
        if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack))
        {
            goto yyoverflow;
        }
        yystate = yytable[yyn];
        *++yystack.s_mark = yytable[yyn];
        *++yystack.l_mark = yylval;
        yychar = YYEMPTY;
        if (yyerrflag > 0)  --yyerrflag;
        goto yyloop;
    }
    if ((yyn = yyrindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
        yyn = yytable[yyn];
        goto yyreduce;
    }
    if (yyerrflag) goto yyinrecovery;

    yyerror("syntax error");

    goto yyerrlab;

yyerrlab:
    ++yynerrs;

yyinrecovery:
    if (yyerrflag < 3)
    {
        yyerrflag = 3;
        for (;;)
        {
            if ((yyn = yysindex[*yystack.s_mark]) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: state %d, error recovery shifting\
 to state %d\n", YYPREFIX, *yystack.s_mark, yytable[yyn]);
#endif
                if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack))
                {
                    goto yyoverflow;
                }
                yystate = yytable[yyn];
                *++yystack.s_mark = yytable[yyn];
                *++yystack.l_mark = yylval;
                goto yyloop;
            }
            else
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: error recovery discarding state %d\n",
                            YYPREFIX, *yystack.s_mark);
#endif
                if (yystack.s_mark <= yystack.s_base) goto yyabort;
                --yystack.s_mark;
                --yystack.l_mark;
            }
        }
    }
    else
    {
        if (yychar == 0) goto yyabort;
#if YYDEBUG
        if (yydebug)
        {
            yys = 0;
            if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
            if (!yys) yys = "illegal-symbol";
            printf("%sdebug: state %d, error recovery discards token %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
        yychar = YYEMPTY;
        goto yyloop;
    }

yyreduce:
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: state %d, reducing by rule %d (%s)\n",
                YYPREFIX, yystate, yyn, yyrule[yyn]);
#endif
    yym = yylen[yyn];
    if (yym)
        yyval = yystack.l_mark[1-yym];
    else
        memset(&yyval, 0, sizeof yyval);
    switch (yyn)
    {
case 1:
#line 93 "src/parser.y"
	{
                    tree* progNode = maketree(PROGRAM);
                    addChild(progNode, yystack.l_mark[0].node);
                    ast = progNode;
                 }
break;
case 2:
#line 101 "src/parser.y"
	{
                    tree* declListNode = maketree(DECLLIST);
                    addChild(declListNode, yystack.l_mark[0].node);
                    yyval.node = declListNode;
                 }
break;
case 3:
#line 107 "src/parser.y"
	{
                    tree* declListNode = maketree(DECLLIST);
                    addChild(declListNode, yystack.l_mark[-1].node);
                    addChild(declListNode, yystack.l_mark[0].node);
                    yyval.node = declListNode;
                 }
break;
case 4:
#line 116 "src/parser.y"
	{
                    tree* declNode = maketree(DECL);
                    addChild(declNode, yystack.l_mark[0].node);
                    yyval.node = declNode;
                 }
break;
case 5:
#line 122 "src/parser.y"
	{
                    tree* declNode = maketree(DECL);
                    addChild(declNode, yystack.l_mark[0].node);
                    yyval.node = declNode;
                 }
break;
case 6:
#line 130 "src/parser.y"
	{
                    tree* varDeclNode = maketree(VARDECL);
                    addChild(varDeclNode, yystack.l_mark[-5].node);
		    if(ST_lookup(yystack.l_mark[-4].strval, scope) != -1)
                      printf("Error: Variable name already used");
		    int index = ST_insert(yystack.l_mark[-4].strval, scope, varDeclNode->children[0]->val, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(varDeclNode, idNode);
                    addChild(varDeclNode, maketreeWithVal(INTEGER, yystack.l_mark[-2].value));
                    yyval.node = varDeclNode;
                 }
break;
case 7:
#line 142 "src/parser.y"
	{
                    tree* varDeclNode = maketree(VARDECL);
                    addChild(varDeclNode, yystack.l_mark[-2].node);
		    if(ST_lookup(yystack.l_mark[-1].strval, scope) != -1)
                      printf("Error: %d: Variable name already used\n", yylineno);
		    int index = ST_insert(yystack.l_mark[-1].strval, scope, varDeclNode->children[0]->val, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(varDeclNode, idNode);
                    yyval.node = varDeclNode;
                 }
break;
case 8:
#line 155 "src/parser.y"
	{
                    yyval.node = maketreeWithVal(TYPESPEC, INT_TYPE);
                 }
break;
case 9:
#line 159 "src/parser.y"
	{
                    yyval.node = maketreeWithVal(TYPESPEC, CHAR_TYPE);
                 }
break;
case 10:
#line 163 "src/parser.y"
	{
                    yyval.node = maketreeWithVal(TYPESPEC, VOID_TYPE);
                 }
break;
case 11:
#line 169 "src/parser.y"
	{
                    tree* funDeclNode = maketree(FUNDECL);
                    addChild(funDeclNode, yystack.l_mark[-6].node);
                    int index = ST_insert(yystack.l_mark[-5].strval, scope, 0, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(funDeclNode, idNode);
                    addChild(funDeclNode, yystack.l_mark[-3].node);
                    yyval.node = funDeclNode;
                 }
break;
case 12:
#line 179 "src/parser.y"
	{
                    tree* funDeclNode = maketree(FUNDECL);
                    addChild(funDeclNode, yystack.l_mark[-5].node);
                    int index = ST_insert(yystack.l_mark[-4].strval, scope, 0, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(funDeclNode, idNode);
                    addChild(funDeclNode, yystack.l_mark[-2].node);
                    addChild(funDeclNode, yystack.l_mark[0].node);
                    yyval.node = funDeclNode;
                 }
break;
case 13:
#line 190 "src/parser.y"
	{
                    tree* funDeclNode = maketree(FUNDECL);
                    addChild(funDeclNode, yystack.l_mark[-4].node);
                    int index = ST_insert(yystack.l_mark[-3].strval, scope, 0, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(funDeclNode, idNode);
                    addChild(funDeclNode, yystack.l_mark[0].node);
                    yyval.node = funDeclNode;
                 }
break;
case 14:
#line 200 "src/parser.y"
	{
                    tree* funDeclNode = maketree(FUNDECL);
                    addChild(funDeclNode, yystack.l_mark[-5].node);
                    int index = ST_insert(yystack.l_mark[-4].strval, scope, 0, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(funDeclNode, idNode);
                    yyval.node = funDeclNode;
                 }
break;
case 15:
#line 211 "src/parser.y"
	{
                    tree* formalDeclListNode = maketree(FORMALDECLLIST);
                    addChild(formalDeclListNode, yystack.l_mark[0].node);
                    yyval.node = formalDeclListNode;
                  }
break;
case 16:
#line 217 "src/parser.y"
	{
                    tree* formalDeclListNode = maketree(FORMALDECLLIST);
                    addChild(formalDeclListNode, yystack.l_mark[-2].node);
                    addChild(formalDeclListNode, yystack.l_mark[0].node);
                    yyval.node = formalDeclListNode;
                  }
break;
case 17:
#line 226 "src/parser.y"
	{
                    tree* formalDeclNode = maketree(FORMALDECL);
                    addChild(formalDeclNode, yystack.l_mark[-1].node);
                    int index = ST_insert(yystack.l_mark[0].strval, scope, 0, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(formalDeclNode, idNode);
                    yyval.node = formalDeclNode;
                 }
break;
case 18:
#line 235 "src/parser.y"
	{
                    tree* formalDeclNode = maketree(FORMALDECL);
                    addChild(formalDeclNode, yystack.l_mark[-3].node);
                    int index = ST_insert(yystack.l_mark[-2].strval, scope, 0, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(formalDeclNode, idNode);
                    yyval.node = formalDeclNode;
                 }
break;
case 19:
#line 246 "src/parser.y"
	{
                    tree* localDeclListNode = maketree(LOCALDECLLIST);
                    addChild(localDeclListNode, yystack.l_mark[0].node);
                    yyval.node = localDeclListNode;
                 }
break;
case 20:
#line 252 "src/parser.y"
	{
                    tree* localDeclListNode = maketree(LOCALDECLLIST);
                    addChild(localDeclListNode, yystack.l_mark[-1].node);
                    addChild(localDeclListNode, yystack.l_mark[0].node);
                    yyval.node = localDeclListNode;
                 }
break;
case 21:
#line 261 "src/parser.y"
	{
                    tree* statementNode = maketree(STATEMENT);
                    addChild(statementNode, yystack.l_mark[0].node);
                    yyval.node = statementNode;
                 }
break;
case 22:
#line 267 "src/parser.y"
	{
                    tree* statementNode = maketree(STATEMENT);
                    addChild(statementNode, yystack.l_mark[0].node);
                    yyval.node = statementNode;
                 }
break;
case 23:
#line 273 "src/parser.y"
	{
                    tree* statementNode = maketree(STATEMENT);
                    addChild(statementNode, yystack.l_mark[0].node);
                    yyval.node = statementNode;
                 }
break;
case 24:
#line 279 "src/parser.y"
	{
                    tree* statementNode = maketree(STATEMENT);
                    addChild(statementNode, yystack.l_mark[0].node);
                    yyval.node = statementNode;
                 }
break;
case 25:
#line 285 "src/parser.y"
	{
                    tree* statementNode = maketree(STATEMENT);
                    addChild(statementNode, yystack.l_mark[0].node);
                    yyval.node = statementNode;
                 }
break;
case 26:
#line 293 "src/parser.y"
	{
                    tree* statementListNode = maketree(STATEMENTLIST);
                    addChild(statementListNode, yystack.l_mark[0].node);
                    yyval.node = statementListNode;
                 }
break;
case 27:
#line 299 "src/parser.y"
	{
                    tree* statementListNode = maketree(STATEMENTLIST);
                    addChild(statementListNode, yystack.l_mark[-1].node);
                    addChild(statementListNode, yystack.l_mark[0].node);
                    yyval.node = statementListNode;
                 }
break;
case 28:
#line 308 "src/parser.y"
	{
                    tree* funBodyNode = maketree(FUNBODY);
                    addChild(funBodyNode, yystack.l_mark[-1].node);
                    yyval.node = funBodyNode;
                 }
break;
case 29:
#line 314 "src/parser.y"
	{
                    tree* funBodyNode = maketree(FUNBODY);
                    addChild(funBodyNode, yystack.l_mark[-1].node);
                    yyval.node = funBodyNode;
                 }
break;
case 30:
#line 320 "src/parser.y"
	{
                    tree* funBodyNode = maketree(FUNBODY);
                    addChild(funBodyNode, yystack.l_mark[-2].node);
                    addChild(funBodyNode, yystack.l_mark[-1].node);
                    yyval.node = funBodyNode;
                 }
break;
case 31:
#line 330 "src/parser.y"
	{
                    tree* compoundStmtNode = maketree(COMPOUNDSTMT);
                    addChild(compoundStmtNode, yystack.l_mark[-1].node);
                    yyval.node = compoundStmtNode;
                 }
break;
case 32:
#line 338 "src/parser.y"
	{
                    tree* assignStmtNode = maketree(ASSIGNSTMT);
                    addChild(assignStmtNode, yystack.l_mark[-3].node);
                    addChild(assignStmtNode, yystack.l_mark[-1].node);
                    yyval.node = assignStmtNode;
                 }
break;
case 33:
#line 345 "src/parser.y"
	{
                    tree* assignStmtNode = maketree(ASSIGNSTMT);
                    addChild(assignStmtNode, yystack.l_mark[-1].node);
                    yyval.node = assignStmtNode;
                 }
break;
case 34:
#line 353 "src/parser.y"
	{
                    tree* condStmtNode = maketree(CONDSTMT);
                    addChild(condStmtNode, yystack.l_mark[-2].node);
                    addChild(condStmtNode, yystack.l_mark[0].node);
                    yyval.node = condStmtNode;
                 }
break;
case 35:
#line 360 "src/parser.y"
	{
                    tree* condStmtNode = maketree(CONDSTMT);
                    addChild(condStmtNode, yystack.l_mark[-4].node);
                    addChild(condStmtNode, yystack.l_mark[-2].node);
                    addChild(condStmtNode, yystack.l_mark[0].node);
                    yyval.node = condStmtNode;
                 }
break;
case 36:
#line 370 "src/parser.y"
	{
                    tree* loopStmtNode = maketree(LOOPSTMT);
                    addChild(loopStmtNode, yystack.l_mark[-2].node);
                    addChild(loopStmtNode, yystack.l_mark[0].node);
                    yyval.node = loopStmtNode;
                 }
break;
case 37:
#line 379 "src/parser.y"
	{
                    yyval.node = maketreeWithVal(RETURNSTMT, yystack.l_mark[-1].value);
                 }
break;
case 38:
#line 383 "src/parser.y"
	{
                    tree* returnStmtNode = maketree(RETURNSTMT);
                    addChild(returnStmtNode, maketreeWithVal(RETURNSTMT, yystack.l_mark[-2].value));
                    addChild(returnStmtNode, yystack.l_mark[-1].node);
                    yyval.node = returnStmtNode;
                 }
break;
case 39:
#line 392 "src/parser.y"
	{
                    tree* varNode = maketree(VAR);
                    int index = ST_insert(yystack.l_mark[0].strval, scope, 0, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(varNode, idNode);
                    yyval.node = varNode;
                 }
break;
case 40:
#line 400 "src/parser.y"
	{  
                    tree* varNode = maketree(VAR);
                    int index = ST_insert(yystack.l_mark[-3].strval, scope, 0, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(varNode, idNode);
                    addChild(varNode, yystack.l_mark[-1].node);
                    yyval.node = varNode;
                 }
break;
case 41:
#line 411 "src/parser.y"
	{
                    tree* exprNode = maketree(EXPRESSION);
                    addChild(exprNode, yystack.l_mark[0].node);
                    yyval.node = exprNode;
                 }
break;
case 42:
#line 417 "src/parser.y"
	{
                    tree* exprNode = maketree(EXPRESSION);
                    addChild(exprNode, yystack.l_mark[-2].node);
                    addChild(exprNode, yystack.l_mark[-1].node);
                    addChild(exprNode, yystack.l_mark[0].node);
                    yyval.node = exprNode;
                 }
break;
case 43:
#line 427 "src/parser.y"
	{
                    
                    yyval.node = maketreeWithVal(RELOP, LTE);
                 }
break;
case 44:
#line 432 "src/parser.y"
	{
                    yyval.node = maketreeWithVal(RELOP, LT);
                 }
break;
case 45:
#line 436 "src/parser.y"
	{
                    yyval.node = maketreeWithVal(RELOP, GT);
                 }
break;
case 46:
#line 440 "src/parser.y"
	{
                    yyval.node = maketreeWithVal(RELOP, GTE);
                 }
break;
case 47:
#line 444 "src/parser.y"
	{
                    yyval.node = maketreeWithVal(RELOP, EQ);
                 }
break;
case 48:
#line 448 "src/parser.y"
	{
                    yyval.node = maketreeWithVal(RELOP, NEQ);
                 }
break;
case 49:
#line 454 "src/parser.y"
	{
                    tree* addExprNode = maketree(ADDEXPR);
                    addChild(addExprNode, yystack.l_mark[0].node);
                    yyval.node = addExprNode;
                 }
break;
case 50:
#line 460 "src/parser.y"
	{
                    tree* addExprNode = maketree(ADDEXPR);
                    addChild(addExprNode, yystack.l_mark[-2].node);
                    addChild(addExprNode, yystack.l_mark[-1].node);
                    addChild(addExprNode, yystack.l_mark[0].node);
                    yyval.node = addExprNode;
                 }
break;
case 51:
#line 470 "src/parser.y"
	{
                    yyval.node = maketreeWithVal(ADDOP, ADD);
                 }
break;
case 52:
#line 474 "src/parser.y"
	{
                    yyval.node = maketreeWithVal(ADDOP, SUB);
                 }
break;
case 53:
#line 480 "src/parser.y"
	{
                    tree* termNode = maketree(TERM);
                    addChild(termNode, yystack.l_mark[0].node);
                    yyval.node = termNode;
                 }
break;
case 54:
#line 486 "src/parser.y"
	{
                    tree* termNode = maketree(TERM);
                    addChild(termNode, yystack.l_mark[-2].node);
                    addChild(termNode, yystack.l_mark[-1].node);
                    addChild(termNode, yystack.l_mark[0].node);
                    yyval.node = termNode;
                 }
break;
case 55:
#line 496 "src/parser.y"
	{
                    yyval.node = maketreeWithVal(MULOP, MUL);
                 }
break;
case 56:
#line 500 "src/parser.y"
	{
                    yyval.node = maketreeWithVal(MULOP, DIV);
                 }
break;
case 57:
#line 506 "src/parser.y"
	{
                    tree* factorNode = maketree(FACTOR);
                    addChild(factorNode, yystack.l_mark[-1].node);
                    yyval.node = factorNode;
                 }
break;
case 58:
#line 512 "src/parser.y"
	{
                    tree* factorNode = maketree(FACTOR);
                    addChild(factorNode, yystack.l_mark[0].node);
                    yyval.node = factorNode;
                 }
break;
case 59:
#line 518 "src/parser.y"
	{
                    tree* factorNode = maketree(FACTOR);
                    addChild(factorNode, yystack.l_mark[0].node);
                    yyval.node = factorNode;
                 }
break;
case 60:
#line 524 "src/parser.y"
	{
                    tree* factorNode = maketree(FACTOR);
                    addChild(factorNode, maketreeWithVal( INTEGER,  yystack.l_mark[0].value ));
                    yyval.node = factorNode;
                 }
break;
case 61:
#line 530 "src/parser.y"
	{
                    yyval.node = maketreeWithVal(FACTOR, yystack.l_mark[0].value);
                 }
break;
case 62:
#line 534 "src/parser.y"
	{
                    yyval.node = maketreeWithVal(FACTOR, yystack.l_mark[0].value);
                 }
break;
case 63:
#line 540 "src/parser.y"
	{
                    tree* funcCallExprNode = maketree(FUNCCALLEXPR);
                    int index = ST_insert(yystack.l_mark[-3].strval, scope, 0, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(funcCallExprNode, idNode);
                    addChild(funcCallExprNode, yystack.l_mark[-1].node);
                    yyval.node = funcCallExprNode;
                 }
break;
case 64:
#line 549 "src/parser.y"
	{
                    tree* funcCallExprNode = maketree(FUNCCALLEXPR);
                    int index = ST_insert(yystack.l_mark[-2].strval, scope, 0, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(funcCallExprNode, idNode);
                    yyval.node = funcCallExprNode;
                 }
break;
case 65:
#line 559 "src/parser.y"
	{
                    tree* argListNode = maketree(ARGLIST);
                    addChild(argListNode, yystack.l_mark[0].node);
                    yyval.node = argListNode;
                 }
break;
case 66:
#line 565 "src/parser.y"
	{
                    tree* argListNode = maketree(ARGLIST);
                    addChild(argListNode, yystack.l_mark[-2].node);
                    addChild(argListNode, yystack.l_mark[0].node);
                    yyval.node = argListNode;
                 }
break;
#line 1161 "obj/y.tab.c"
    }
    yystack.s_mark -= yym;
    yystate = *yystack.s_mark;
    yystack.l_mark -= yym;
    yym = yylhs[yyn];
    if (yystate == 0 && yym == 0)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: after reduction, shifting from state 0 to\
 state %d\n", YYPREFIX, YYFINAL);
#endif
        yystate = YYFINAL;
        *++yystack.s_mark = YYFINAL;
        *++yystack.l_mark = yyval;
        if (yychar < 0)
        {
            if ((yychar = YYLEX) < 0) yychar = 0;
#if YYDEBUG
            if (yydebug)
            {
                yys = 0;
                if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
                if (!yys) yys = "illegal-symbol";
                printf("%sdebug: state %d, reading %d (%s)\n",
                        YYPREFIX, YYFINAL, yychar, yys);
            }
#endif
        }
        if (yychar == 0) goto yyaccept;
        goto yyloop;
    }
    if ((yyn = yygindex[yym]) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn];
    else
        yystate = yydgoto[yym];
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: after reduction, shifting from state %d \
to state %d\n", YYPREFIX, *yystack.s_mark, yystate);
#endif
    if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack))
    {
        goto yyoverflow;
    }
    *++yystack.s_mark = (short) yystate;
    *++yystack.l_mark = yyval;
    goto yyloop;

yyoverflow:
    yyerror("yacc stack overflow");

yyabort:
    yyfreestack(&yystack);
    return (1);

yyaccept:
    yyfreestack(&yystack);
    return (0);
}
