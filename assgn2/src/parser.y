%{
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<../src/tree.h>
#include<../src/strtab.h>

extern int yylineno;

/* NOTE: mC has two kinds of scopes for variables : local and global. Variables declared outside any
function are considered globals, whereas variables (and parameters) declared inside a function foo are local to foo. You should update the
scope variable whenever you are inside a production that matches function definition (funDecl production). The rationale is that you are
entering that function, so all variables, arrays, and other functions should be within this scope. You should pass this variable whenever
you are calling the ST_insert or ST_lookup functions. This variable should be updated to scope = "" to indicate global scope whenever funDecl finishes. Treat these hints as helpful directions only. You may implement all of the functions as you like and not adhere to my instructions. As long as the directory structure is correct and the file names are correct, we are okay with it. */
char* scope = "";
%}

/* the union describes the fields available in the yylval variable */
%union
{
    int value;
    struct treenode *node;
    char *strval;
}

/*Add token declarations below. The type <value> indicates that the associated token will be of a value type such as integer, float etc., and <value> indicates that the associated token will be of string type.*/
%token <strval> ID
%token <value> INTCONST
%token <value> CHARCONST
%token <value> STRCONST

/* keywords */
%token <value> KWD_IF
%token <value> KWD_ELSE
%token <value> KWD_WHILE
%token <value> KWD_INT
%token <value> KWD_STRING
%token <value> KWD_CHAR
%token <value> KWD_RETURN
%token <value> KWD_VOID

/* operators */
%token <value> OPER_ADD
%token <value> OPER_SUB
%token <value> OPER_MUL
%token <value> OPER_DIV
%token <value> OPER_LT
%token <value> OPER_GT
%token <value> OPER_GTE
%token <value> OPER_LTE
%token <value> OPER_EQ
%token <value> OPER_NEQ
%token <value> OPER_ASGN
%token <value> OPER_INC
%token <value> OPER_DEC
%token <value> OPER_AND
%token <value> OPER_OR
%token <value> OPER_NOT
%token <value> OPER_AT
%token <value> OPER_MOD
%token <value> WHITESPACE
%token <value> NEWLINE

/* brackets & parens */
%token <value> LSQ_BRKT
%token <value> RSQ_BRKT
%token <value> LCRLY_BRKT
%token <value> RCRLY_BRKT
%token <value> LPAREN
%token <value> RPAREN

/* punctuation */
%token <value> COMMA
%token <value> SEMICLN

/* other stuff */
%token <value> ILLEGAL_TOKEN
%token <value> ERROR


/* Declare non-terminal symbols as of type node. Provided below is one example. node is defined as 'struct treenode *node' in the above union data structure. This declaration indicates to parser that these non-terminal variables will be implemented using a 'treenode *' type data structure. Hence, the circles you draw when drawing a parse tree, the following lines are telling yacc that these will eventually become circles in an AST. This is one of the connections between the AST you draw by hand and how yacc implements code to concretize that. We provide with two examples: program and declList from the grammar. Make sure to add the rest.  */

%type <node> program declList typeSpecifier decl varDecl funDecl formalDeclList formalDecl funBody localDeclList statementList statement compoundStmt assignStmt condStmt loopStmt returnStmt var expression relop addExpr addop term mulop factor funcCallExpr argList


%start program

%%
/* TODO: Your grammar and semantic actions go here. We provide with two example productions and their associated code for adding non-terminals to the AST.*/


program         : declList
                 {
                    tree* progNode = maketree(PROGRAM);
                    addChild(progNode, $1);
                    ast = progNode;
                 }
                ;

declList        : decl
                 {
                    tree* declListNode = maketree(DECLLIST);
                    addChild(declListNode, $1);
                    $$ = declListNode;
                 }
                | declList decl
                 {
                    tree* declListNode = maketree(DECLLIST);
                    addChild(declListNode, $1);
                    addChild(declListNode, $2);
                    $$ = declListNode;
                 }
                ;

decl            : varDecl
                 {
                    tree* declNode = maketree(DECL);
                    addChild(declNode, $1);
                    $$ = declNode;
                 }
                | funDecl
                 {
                    tree* declNode = maketree(DECL);
                    addChild(declNode, $1);
                    $$ = declNode;
                 }
                ;

varDecl         : typeSpecifier ID LSQ_BRKT INTCONST RSQ_BRKT SEMICLN
                 {
                    tree* varDeclNode = maketree(VARDECL);
                    addChild(varDeclNode, $1);
		    if(ST_lookup($2, scope) != -1)
                      printf("Error: Variable name already used");
		    int index = ST_insert($2, scope, varDeclNode->children[0]->val, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(varDeclNode, idNode);
                    addChild(varDeclNode, maketreeWithVal(INTEGER, $4));
                    $$ = varDeclNode;
                 }
                | typeSpecifier ID SEMICLN
                 {
                    tree* varDeclNode = maketree(VARDECL);
                    addChild(varDeclNode, $1);
		    if(ST_lookup($2, scope) != -1)
                      printf("Error: %d: Variable name already used\n", yylineno);
		    int index = ST_insert($2, scope, varDeclNode->children[0]->val, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(varDeclNode, idNode);
                    $$ = varDeclNode;
                 }
                ;

typeSpecifier   : KWD_INT
                 {
                    $$ = maketreeWithVal(TYPESPEC, INT_TYPE);
                 }
                | KWD_CHAR
                 {
                    $$ = maketreeWithVal(TYPESPEC, CHAR_TYPE);
                 }
                | KWD_VOID
                 {
                    $$ = maketreeWithVal(TYPESPEC, VOID_TYPE);
                 }
                ;

funDecl         : typeSpecifier ID LPAREN formalDeclList RPAREN LCRLY_BRKT RCRLY_BRKT
                 {
                    tree* funDeclNode = maketree(FUNDECL);
                    addChild(funDeclNode, $1);
                    int index = ST_insert($2, scope, 0, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(funDeclNode, idNode);
                    addChild(funDeclNode, $4);
                    $$ = funDeclNode;
                 }
                |  typeSpecifier ID LPAREN formalDeclList RPAREN funBody
                 {
                    tree* funDeclNode = maketree(FUNDECL);
                    addChild(funDeclNode, $1);
                    int index = ST_insert($2, scope, 0, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(funDeclNode, idNode);
                    addChild(funDeclNode, $4);
                    addChild(funDeclNode, $6);
                    $$ = funDeclNode;
                 }
                | typeSpecifier ID LPAREN RPAREN funBody
                 {
                    tree* funDeclNode = maketree(FUNDECL);
                    addChild(funDeclNode, $1);
                    int index = ST_insert($2, scope, 0, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(funDeclNode, idNode);
                    addChild(funDeclNode, $5);
                    $$ = funDeclNode;
                 } 
                | typeSpecifier ID LPAREN RPAREN LCRLY_BRKT RCRLY_BRKT
                 {
                    tree* funDeclNode = maketree(FUNDECL);
                    addChild(funDeclNode, $1);
                    int index = ST_insert($2, scope, 0, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(funDeclNode, idNode);
                    $$ = funDeclNode;
                 } 
                ;

formalDeclList  : formalDecl
                  {
                    tree* formalDeclListNode = maketree(FORMALDECLLIST);
                    addChild(formalDeclListNode, $1);
                    $$ = formalDeclListNode;
                  }
                | formalDecl COMMA formalDeclList
                  {
                    tree* formalDeclListNode = maketree(FORMALDECLLIST);
                    addChild(formalDeclListNode, $1);
                    addChild(formalDeclListNode, $3);
                    $$ = formalDeclListNode;
                  }
                ;

formalDecl      : typeSpecifier ID
                 {
                    tree* formalDeclNode = maketree(FORMALDECL);
                    addChild(formalDeclNode, $1);
                    int index = ST_insert($2, scope, 0, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(formalDeclNode, idNode);
                    $$ = formalDeclNode;
                 }
                | typeSpecifier ID LSQ_BRKT RSQ_BRKT
                 {
                    tree* formalDeclNode = maketree(FORMALDECL);
                    addChild(formalDeclNode, $1);
                    int index = ST_insert($2, scope, 0, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(formalDeclNode, idNode);
                    $$ = formalDeclNode;
                 }
                ;

localDeclList   : varDecl
                 {
                    tree* localDeclListNode = maketree(LOCALDECLLIST);
                    addChild(localDeclListNode, $1);
                    $$ = localDeclListNode;
                 }
                | varDecl localDeclList
                 {
                    tree* localDeclListNode = maketree(LOCALDECLLIST);
                    addChild(localDeclListNode, $1);
                    addChild(localDeclListNode, $2);
                    $$ = localDeclListNode;
                 }
                ;

statement       : compoundStmt
                 {
                    tree* statementNode = maketree(STATEMENT);
                    addChild(statementNode, $1);
                    $$ = statementNode;
                 }
                | assignStmt
                 {
                    tree* statementNode = maketree(STATEMENT);
                    addChild(statementNode, $1);
                    $$ = statementNode;
                 } 
                | condStmt
                 {
                    tree* statementNode = maketree(STATEMENT);
                    addChild(statementNode, $1);
                    $$ = statementNode;
                 } 
                | loopStmt
                 {
                    tree* statementNode = maketree(STATEMENT);
                    addChild(statementNode, $1);
                    $$ = statementNode;
                 } 
                | returnStmt
                 {
                    tree* statementNode = maketree(STATEMENT);
                    addChild(statementNode, $1);
                    $$ = statementNode;
                 }
                ;

statementList   :  statement
                 {
                    tree* statementListNode = maketree(STATEMENTLIST);
                    addChild(statementListNode, $1);
                    $$ = statementListNode;
                 }
                |  statement statementList
                 {
                    tree* statementListNode = maketree(STATEMENTLIST);
                    addChild(statementListNode, $1);
                    addChild(statementListNode, $2);
                    $$ = statementListNode;
                 }
                ;

funBody         :  LCRLY_BRKT statementList RCRLY_BRKT
                 {
                    tree* funBodyNode = maketree(FUNBODY);
                    addChild(funBodyNode, $2);
                    $$ = funBodyNode;
                 }
                |  LCRLY_BRKT localDeclList RCRLY_BRKT
                 {
                    tree* funBodyNode = maketree(FUNBODY);
                    addChild(funBodyNode, $2);
                    $$ = funBodyNode;
                 }
                |  LCRLY_BRKT localDeclList statementList RCRLY_BRKT
                 {
                    tree* funBodyNode = maketree(FUNBODY);
                    addChild(funBodyNode, $2);
                    addChild(funBodyNode, $3);
                    $$ = funBodyNode;
                 }
                ;


compoundStmt    : LCRLY_BRKT statementList RCRLY_BRKT
                 {
                    tree* compoundStmtNode = maketree(COMPOUNDSTMT);
                    addChild(compoundStmtNode, $2);
                    $$ = compoundStmtNode;
                 }
                ;

assignStmt      : var OPER_ASGN expression SEMICLN 
                 {
                    tree* assignStmtNode = maketree(ASSIGNSTMT);
                    addChild(assignStmtNode, $1);
                    addChild(assignStmtNode, $3);
                    $$ = assignStmtNode;
                 }
                | expression SEMICLN
                 {
                    tree* assignStmtNode = maketree(ASSIGNSTMT);
                    addChild(assignStmtNode, $1);
                    $$ = assignStmtNode;
                 }
                ;

condStmt        : KWD_IF LPAREN expression RPAREN statement
                 {
                    tree* condStmtNode = maketree(CONDSTMT);
                    addChild(condStmtNode, $3);
                    addChild(condStmtNode, $5);
                    $$ = condStmtNode;
                 }
                | KWD_IF LPAREN expression RPAREN statement KWD_ELSE statement
                 {
                    tree* condStmtNode = maketree(CONDSTMT);
                    addChild(condStmtNode, $3);
                    addChild(condStmtNode, $5);
                    addChild(condStmtNode, $7);
                    $$ = condStmtNode;
                 }
                ;

loopStmt        : KWD_WHILE LPAREN expression RPAREN statement
                 {
                    tree* loopStmtNode = maketree(LOOPSTMT);
                    addChild(loopStmtNode, $3);
                    addChild(loopStmtNode, $5);
                    $$ = loopStmtNode;
                 }
                ;

returnStmt      : KWD_RETURN SEMICLN
                 {
                    $$ = maketreeWithVal(RETURNSTMT, $1);
                 }
                | KWD_RETURN expression SEMICLN
                 {
                    tree* returnStmtNode = maketree(RETURNSTMT);
                    addChild(returnStmtNode, maketreeWithVal(RETURNSTMT, $1));
                    addChild(returnStmtNode, $2);
                    $$ = returnStmtNode;
                 }
                ;

var             : ID
                 {
                    tree* varNode = maketree(VAR);
                    int index = ST_insert($1, scope, 0, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(varNode, idNode);
                    $$ = varNode;
                 }
                | ID LSQ_BRKT addExpr RSQ_BRKT
                 {  
                    tree* varNode = maketree(VAR);
                    int index = ST_insert($1, scope, 0, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(varNode, idNode);
                    addChild(varNode, $3);
                    $$ = varNode;
                 }
                ;

expression      : addExpr
                 {
                    tree* exprNode = maketree(EXPRESSION);
                    addChild(exprNode, $1);
                    $$ = exprNode;
                 }
                | expression relop addExpr
                 {
                    tree* exprNode = maketree(EXPRESSION);
                    addChild(exprNode, $1);
                    addChild(exprNode, $2);
                    addChild(exprNode, $3);
                    $$ = exprNode;
                 }
                ;

relop           : OPER_LTE
                 {
                    
                    $$ = maketreeWithVal(RELOP, LTE);
                 }
                | OPER_LT 
                 {
                    $$ = maketreeWithVal(RELOP, LT);
                 }
                | OPER_GT 
                 {
                    $$ = maketreeWithVal(RELOP, GT);
                 }
                | OPER_GTE 
                 {
                    $$ = maketreeWithVal(RELOP, GTE);
                 }
                | OPER_EQ 
                 {
                    $$ = maketreeWithVal(RELOP, EQ);
                 }
                | OPER_NEQ 
                 {
                    $$ = maketreeWithVal(RELOP, NEQ);
                 }
                ;

addExpr         : term
                 {
                    tree* addExprNode = maketree(ADDEXPR);
                    addChild(addExprNode, $1);
                    $$ = addExprNode;
                 }
                | addExpr addop term
                 {
                    tree* addExprNode = maketree(ADDEXPR);
                    addChild(addExprNode, $1);
                    addChild(addExprNode, $2);
                    addChild(addExprNode, $3);
                    $$ = addExprNode;
                 }
                ;

addop           : OPER_ADD
                 {
                    $$ = maketreeWithVal(ADDOP, ADD);
                 }
                | OPER_SUB
                 {
                    $$ = maketreeWithVal(ADDOP, SUB);
                 }
                ;

term            : factor
                 {
                    tree* termNode = maketree(TERM);
                    addChild(termNode, $1);
                    $$ = termNode;
                 }
                | term mulop factor
                 {
                    tree* termNode = maketree(TERM);
                    addChild(termNode, $1);
                    addChild(termNode, $2);
                    addChild(termNode, $3);
                    $$ = termNode;
                 }
                ;

mulop           : OPER_MUL
                 {
                    $$ = maketreeWithVal(MULOP, MUL);
                 }
                | OPER_DIV
                 {
                    $$ = maketreeWithVal(MULOP, DIV);
                 }
                ;

factor          : LPAREN expression RPAREN
                 {
                    tree* factorNode = maketree(FACTOR);
                    addChild(factorNode, $2);
                    $$ = factorNode;
                 }
                | var
                 {
                    tree* factorNode = maketree(FACTOR);
                    addChild(factorNode, $1);
                    $$ = factorNode;
                 }
                | funcCallExpr
                 {
                    tree* factorNode = maketree(FACTOR);
                    addChild(factorNode, $1);
                    $$ = factorNode;
                 }
                | INTCONST
                 {
                    tree* factorNode = maketree(FACTOR);
                    addChild(factorNode, maketreeWithVal( INTEGER,  $1 ));
                    $$ = factorNode;
                 }
                | CHARCONST
                 {
                    $$ = maketreeWithVal(FACTOR, $1);
                 }
                | STRCONST
                 {
                    $$ = maketreeWithVal(FACTOR, $1);
                 }
                ;

funcCallExpr    : ID LPAREN argList RPAREN 
                 {
                    tree* funcCallExprNode = maketree(FUNCCALLEXPR);
                    int index = ST_insert($1, scope, 0, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(funcCallExprNode, idNode);
                    addChild(funcCallExprNode, $3);
                    $$ = funcCallExprNode;
                 }
                | ID LPAREN RPAREN
                 {
                    tree* funcCallExprNode = maketree(FUNCCALLEXPR);
                    int index = ST_insert($1, scope, 0, 0);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(funcCallExprNode, idNode);
                    $$ = funcCallExprNode;
                 }
                ;

argList         : expression
                 {
                    tree* argListNode = maketree(ARGLIST);
                    addChild(argListNode, $1);
                    $$ = argListNode;
                 }
                | argList COMMA expression
                 {
                    tree* argListNode = maketree(ARGLIST);
                    addChild(argListNode, $1);
                    addChild(argListNode, $3);
                    $$ = argListNode;
                 }
                ;
%%

int yywarning(char * msg){
    printf("warning: line %d: %s\n", yylineno, msg);
   return 0;
}

int yyerror(char * msg){
    printf("error: line %d: %s\n", yylineno, msg);
    return 0;
}
