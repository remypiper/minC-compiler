#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<../src/tree.h>
#include<../src/strtab.h>

int main() {

// Initialize symbol table to null.
for (int i = 0; i < MAXIDS; i++) {
    strTable[i] = (struct strEntry*) malloc(sizeof(struct strEntry)); // Go ahead and allocate all 1000 entries. Could be better.
    strTable[i]->id = NULL;
    strTable[i]->scope = NULL;
    strTable[i]->data_type = -1;
    strTable[i]->symbol_type = -1;
}

if (!yyparse()){
        printAst(ast, 1);

        printf("\n\nSYMBOL TABLE:\n");
        for(int i = 0; i < MAXIDS; i++){
            if(strTable[i]->id != NULL){
                output_entry(i);
            }
        }
    }
    return 0;
}
