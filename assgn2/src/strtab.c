#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<strtab.h>

/* Provided is a hash function that you may call to get an integer back. */
unsigned long hash(unsigned char *str) {
    unsigned long hash = 5381;
    int c;

    while (c = *str++)
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash;
}

int ST_insert(char *id, char *scope, int data_type, int symbol_type) {
    // Allocate memory for a new entry, and put data into that object
    struct strEntry *newEntry = (struct strEntry*) malloc(sizeof(struct strEntry));
    newEntry->id = id;
    newEntry->scope = scope;
    newEntry->data_type = data_type;
    newEntry->symbol_type = symbol_type;

    // Concatenate the scope and id and use that to create the hash key
    char str1[50];
    char str2[50];
    strcpy(str1, id);
    strcpy(str2, scope);
    char* key = strncat(str1, str2, 100);
    int index = hash(key) % 1000;
    
    
// TODO: Use ST_lookup to check if the id is already in the symbol table. If yes, ST_lookup will return an index that is not -1. if index != -1, that means the variable is already in the hashtable. Hence, no need to insert that variable again. However, if index == -1, then use linear probing to find an empty spot and insert there. Then return that index.
    if (ST_lookup(id, scope) == -1) { // id is not already in the hash table
        while (strTable[index]) {
            if (strTable[index]->id == NULL) { // We have found an empty spot
                strTable[index] = newEntry; // Put our new object at that address.
                // free(newEntry); TODO: Figure out why we can't free here.
                break;
            }
            else { index++; }
        }
    }
    else
        return ST_lookup(id, scope); // Returns the index of the value, or -1 if the value was already in the table.
}

int ST_lookup(char *id, char *scope) {
    // Concatenate the scope and id and use that to create the hash key
    char str1[50];
    char str2[50];
    strcpy(str1, id);
    strcpy(str2, scope);
    char* key = strncat(str1, str2, 100);
    int index = hash(key) % 1000;
    
    // TODO: Use the hash value to check if the index position has the "id". If not, keep looking for id until you find an empty spot. If you find "id", return that index. If you arrive at an empty spot, that means "id" is not there. Then return -1. 
    while (strTable[index]->id != NULL) {
        if (strTable[index]->id == id) {
            return index;
        }
        else { index++; } // If it isn't there, then it's either a few spots ahead, or not in the table. If we reach null before finding it, then it isn't in the table.
    }
    return -1; // Only reached if we find an empty space somewhere in front of the initial index.
}

char* ST_getId(int index) { // Returns the value given a key
     return strTable[index]->id;
}

char *  dataType[3] = {"Int", "Char", "Void"};
char * symbolType[3] = {"Scalar", "Array", "Function"};

void output_entry(int i) {
    printf("%d: %s ", i, dataType[strTable[i]->data_type]);
    printf("%s: %s %s \n", strTable[i]->scope, strTable[i]->id, symbolType[strTable[i]->symbol_type]);
}
