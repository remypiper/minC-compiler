#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <tree.h>   // definition for nodes of the AST
#include <strtab.h> // the symbol table for needed information about nodes
#include <gencode.h>

// a counter for jump labels to be made unique
int labelcounter=0;
// // reset register counter (done after calling a func or at beginning of func)
int registercounter=7;
//
// // defines to get the next label counter or to reset registers
// #define nextlabel() (++labelcounter)
// #define resetregisters() (registercounter=7)

void gencode(FILE* outfile, tree* node, int nestLevel) {
    if (node->nodeKind == ASSIGNSTMT) {
        emitAssignStatement(outfile, node);
        for (int i = 0; i < node->numChildren; i++)  {
            gencode(outfile, getChild(node, i), nestLevel + 1);
        }
    }

    else if (node->nodeKind == CONDSTMT) {
        int label = ++labelcounter;
        emitCondStatement(outfile, node);
        for (int i = 0; i < node->numChildren; i++)  {
            gencode(outfile, getChild(node, i), nestLevel + 1);
        }
        fprintf(outfile, "label%d:\n", label);
    }

    else if (node->nodeKind == LOOPSTMT) {
        int label = ++labelcounter;
        fprintf(outfile, "While:\n");
        emitLoopStatement(outfile, node);
        for (int i = 0; i < node->numChildren; i++)  {
            gencode(outfile, getChild(node, i), nestLevel + 1);
        }
        fprintf(outfile, "\tj While\n");
        fprintf(outfile, "label%d:\n", label);
    }

    // The declaration of main is assembled without needing to be called.
    else if (node->nodeKind == FUNDECL || node->nodeKind == FUNCCALLEXPR) {
        emitFunc(outfile, node);
        for (int i = 0; i < node->numChildren; i++)  {
            gencode(outfile, getChild(node, i), nestLevel + 1);
        }
    }

    // if (node->nodeKind == ) Iterative statement???
    else {
        for (int i = 0; i < node->numChildren; i++)  {
            gencode(outfile, getChild(node, i), nestLevel + 1);
        }
    }
}

/*
Number  | Name      | Description
0       | $zero     | The value 0
2-3     | $v0 - $v1 | (values) from expression evaluation and function results
4-7     | $a0 - $a3 | (arguments) First four parameters for subroutine
8-15    | $t0 - $t7 | Temporary variables
16-23   | $s0 - $s7 | Saved values representing final computed results
24-25   | $t8 - $t9 | Temporary variables2
31      | $ra       |Return address
*/

char *registernames[] = { "$zero","$at","$v0","$v1","$a0","$a1","$a2","$a3",
                          "$t0","$t1","$t2","$t3","$t4","$t5","$t6","$t7",
                          "$s0","$s1","$s2","$s3","$s4","$s5","$s6","$s7" };
int expr(FILE* outfile, tree* node){ 
  int result, t1, t2;
  // int labelno = 0;
  char* op;
  switch (node->nodeKind){
    case ADDOP:
      t1 = expr(outfile, node->children[0]);
      t2 = expr(outfile, node->children[1]);
      result = nextReg();
      op = "sub";
      if (node->val == ADD)
        op = "add";
      fprintf(outfile, "\t%s %s, %s, %s\n", op, registernames[result], registernames[t1], registernames[t2]);  
      break;
    case RELOP:
        t1 = expr(outfile, node->children[0]);
        t2 = expr(outfile, node->children[1]);
        result = nextReg();
        if (node->val == LT) {
            op = "bge";
        }
        else if (node->val == GT) {
            op = "ble";
        }
        else if (node->val == EQ) {
            op = "bne";
        }
        fprintf(outfile, "\t%s %s, %s, %s%d\n", op, registernames[t1], registernames[t2], "label", labelcounter);
        //fprintf(outfile, "label%d:\n", labelno);
        //labelno++;
        break;
    case MULOP:
      t1 = expr(outfile, node->children[0]);
      t2 = expr(outfile, node->children[1]);
      result = nextReg();
       op = "div";
      if (node->val == MUL)
        op = "mul"; 
      fprintf(outfile, "\t%s %s, %s, %s\n", op, registernames[result], registernames[t1], registernames[t2]);  
      break;
    case VAR:
      result = nextReg();
      fprintf(outfile, "\tlw %s, %s\n", registernames[result], ST_getId(node->children[0]->val));  
      break;
    case FACTOR:
      result = nextReg();
      fprintf(outfile, "\tli %s, %d\n", registernames[result], node->children[0]->val);  
      break;
    case FUNDECL:
       if (strcmp(ST_getId(node->children[1]->val), "main") == 0) {
            fprintf(outfile,"%s:\n", ST_getId(node->children[1]->val)); // Print function label
        }
        break;
    case FUNCCALLEXPR:
       if (strcmp(ST_getId(node->children[0]->val), "output") == 0) {
            fprintf(outfile, "\tli $v0, 1     # print integer\n");
            fprintf(outfile, "\tli $a0, %d\n", node->children[1]->val); // Improprer load here.
           fprintf(outfile, "\tsyscall\n");
        }
        else {
            fprintf(outfile,"%s:\n", ST_getId(node->children[0]->val)); // Print function label
        }
        break;
  }  

  return result; 

}

void emitAssignStatement(FILE* outfile, tree* node) {
  int r1, r2;
  r2 = expr(outfile, node->children[1]);
  fprintf(outfile, "\tsw %s, %s\n", registernames[r2], ST_getId(node->children[0]->children[0]->val));  
}

void emitCondStatement(FILE* outfile, tree* node) {
    int r1, r2;
    r1 = expr(outfile, node->children[0]);
    r2 = expr(outfile, node->children[1]);
}

void emitLoopStatement(FILE* outfile, tree* node) {
    int r1, r2;
    r1 = expr(outfile, node->children[0]);
    r2 = expr(outfile, node->children[1]);
}

void emitFunc(FILE* outfile, tree* node) {
    int r1, r2;
    r1 = expr(outfile, node);
}

// get the next available register
int nextReg(){
    if (++registercounter == 16) registercounter = 8;
    return registercounter;
}
