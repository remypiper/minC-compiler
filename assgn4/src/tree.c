#include<tree.h>
#include<strtab.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

/* string values for ast node types, makes tree output more readable */
char *nodeNames[] = {"PROGRAM", "DECLLIST", "DECL", "VARDECL", "TYPESPEC", "FUNDECL",
                "FORMALDECLLIST", "FORMALDECL", "FUNBODY", "LOCALDECLLIST",
                "STATEMENTLIST", "STATEMENT", "COMPOUNDSTMT", "ASSIGNSTMT",
                "CONDSTMT", "LOOPSTMT", "RETURNSTMT", "EXPRESSION", "RELOP",
                "ADDEXPR", "ADDOP", "TERM", "MULOP", "FACTOR", "FUNCCALLEXPR",
                "ARGLIST", "INTEGER", "IDENTIFIER", "VAR", "ARRAYDECL", "CHAR",
                "FUNCTYPENAME"};

tree *maketree(int kind) {
  tree *this = (tree *) malloc(sizeof(struct treenode));
  this->nodeKind = kind;
  this->numChildren = 0;
  return this;

}

tree *maketreeWithVal(int kind, int val) {
  tree *this = (tree *) malloc(sizeof(struct treenode));
  this->nodeKind = kind;
  this->numChildren = 0;
  this->val = val;
  return this;

}


void addChild(tree *parent, tree *child) {
  if (parent->numChildren == MAXCHILDREN) {
    printf("Cannot add child to parent node\n");
    exit(1);
  }
  nextAvailChild(parent) = child;
  parent->numChildren++;
}

void printAst(tree *node, int nestLevel) {

    if (node->nodeKind == INTEGER) {
	    printf("INTEGER, %d\n", node->val);
    }    
    
    else if (node->nodeKind == IDENTIFIER && node->val != -1) {
	    printf("IDENTIFIER, %s\n", ST_getId(node->val));
    }
    
    else if (node->nodeKind == TYPESPEC) {
        if (node->val == INT_TYPE)
	        printf("TYPESPEC, int\n");
        if (node->val == CHAR_TYPE)
	        printf("TYPESPEC, char\n");
        if (node->val == VOID_TYPE)
	        printf("TYPESPEC, void\n");
    }    
    
    else if (node->nodeKind == ADDOP) {
        if (node->val == ADD)
	        printf("ADDOP, +\n");
        if (node->val == SUB)
	        printf("ADDOP, -\n");
    }
    
    else if (node->nodeKind == MULOP) {
        if (node->val == MUL)
	        printf("MULOP, *\n");
        if (node->val == DIV)
	        printf("MULOP, /\n");
    }
    
    else if (node->nodeKind == RELOP) {
        if (node->val == LT)
	        printf("RELOP, <\n");
        if (node->val == LTE)
	        printf("RELOP, <=\n");
        if (node->val == EQ)
	        printf("RELOP, ==\n");
        if (node->val == GTE)
	        printf("RELOP, >=\n");
        if (node->val == GT)
	        printf("RELOP, >\n");
        if (node->val == NEQ)
	        printf("RELOP, !=\n");
    }
 
    else if (node->nodeKind == FACTOR) {
        printf("FACTOR\n");
    }
    
    else {
	    printf("%s\n", nodeNames[node->nodeKind]);
    }

  int i, j;

  for (i = 0; i < node->numChildren; i++)  {
    for (j = 0; j < nestLevel; j++) 
      printf("    ");
    printAst(getChild(node, i), nestLevel + 1);
  }
}
