%{
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<../src/tree.h>
#include<../src/strtab.h>

extern int yylineno;

/* NOTE: mC has two kinds of scopes for variables : local and global. Variables declared outside any
function are considered globals, whereas variables (and parameters) declared inside a function foo are local to foo. You should update the
scope variable whenever you are inside a production that matches function definition (funDecl production). The rationale is that you are
entering that function, so all variables, arrays, and other functions should be within this scope. You should pass this variable whenever
you are calling the ST_insert or ST_lookup functions. This variable should be updated to scope = "" to indicate global scope whenever funDecl finishes. Treat these hints as helpful directions only. You may implement all of the functions as you like and not adhere to my instructions. As long as the directory structure is correct and the file names are correct, we are okay with it. */
char* scope = "global";
char scopeName[STRSIZE];
int scopeLevel = 0;
tree *funDeclNode;
int numArgs = 0;
%}

/* the union describes the fields available in the yylval variable */
%union
{
    int value;
    struct treenode *node;
    char *strval;
}

/*Add token declarations below. The type <value> indicates that the associated token will be of a value type such as integer, float etc., and <value> indicates that the associated token will be of string type.*/
%token <strval> ID
%token <value> INTCONST
%token <value> CHARCONST
%token <value> STRCONST

/* keywords */
%token <value> KWD_IF
%token <value> KWD_ELSE
%token <value> KWD_WHILE
%token <value> KWD_INT
%token <value> KWD_STRING
%token <value> KWD_CHAR
%token <value> KWD_RETURN
%token <value> KWD_VOID

/* operators */
%token <value> OPER_ADD
%token <value> OPER_SUB
%token <value> OPER_MUL
%token <value> OPER_DIV
%token <value> OPER_LT
%token <value> OPER_GT
%token <value> OPER_GTE
%token <value> OPER_LTE
%token <value> OPER_EQ
%token <value> OPER_NEQ
%token <value> OPER_ASGN
%token <value> OPER_INC
%token <value> OPER_DEC
%token <value> OPER_AND
%token <value> OPER_OR
%token <value> OPER_NOT
%token <value> OPER_AT
%token <value> OPER_MOD
%token <value> WHITESPACE
%token <value> NEWLINE

/* brackets & parens */
%token <value> LSQ_BRKT
%token <value> RSQ_BRKT
%token <value> LCRLY_BRKT
%token <value> RCRLY_BRKT
%token <value> LPAREN
%token <value> RPAREN

/* punctuation */
%token <value> COMMA
%token <value> SEMICLN

/* other stuff */
%token <value> ILLEGAL_TOKEN
%token <value> ERROR


/* Declare non-terminal symbols as of type node. Provided below is one example. node is defined as 'struct treenode *node' in the above union data structure. This declaration indicates to parser that these non-terminal variables will be implemented using a 'treenode *' type data structure. Hence, the circles you draw when drawing a parse tree, the following lines are telling yacc that these will eventually become circles in an AST. This is one of the connections between the AST you draw by hand and how yacc implements code to concretize that. We provide with two examples: program and declList from the grammar. Make sure to add the rest.  */

%type <node> program declList typeSpecifier decl varDecl funDecl formalDeclList formalDecl funBody localDeclList statementList statement compoundStmt assignStmt condStmt loopStmt returnStmt var expression relop addExpr addop term mulop factor funcCallExpr argList


%start program

%%
/* TODO: Your grammar and semantic actions go here. We provide with two example productions and their associated code for adding non-terminals to the AST.*/


program         : declList
                 {
                    //tree* progNode = maketree(PROGRAM);
                    //addChild(progNode, $1);
                    //ast = progNode;
		    ast = $1;
                 }
                ;

declList        : decl
                 {
		    $$  =  maketree(DECLLIST); 
                    addChild($$, $1);
                 }
                | declList decl
                 {
                   // tree* declListNode = maketree(DECLLIST);
                   // addChild(declListNode, $1);
                   // addChild(declListNode, $2);
                   // $$ = declListNode;
		   $$ = $1;
		   addChild($$, $2);
                 }
                ;

decl            : varDecl
                 {
                    $$ = $1;
                 }
                | funDecl
                 {
                    $$ = $1;
                 }
                ;

varDecl         : typeSpecifier ID LSQ_BRKT INTCONST  RSQ_BRKT SEMICLN
                 {  
                  int type = 0;
                  if(ST_lookup($2, scope) != -1)
                      printf("Error: %d: Variable name already used\n", yylineno);
                  else if($4 < 1)
                      printf("Error: %d: Array variable declared with size less than 1\n", yylineno);
                      tree* varDeclNode = maketree(VARDECL);
                      addChild(varDeclNode, $1);
                  switch(varDeclNode->children[0]->nodeKind) {
                      case INT_TYPE:
                      type = INT_ARR;
                      break;
                   case CHAR_TYPE:
                      type = CHAR_ARR;
                      break;
                   case VOID_TYPE:
                      type = VOID_ARR;
                      break;
                   default:
                      type = 0;
            }
                   int index = ST_insert($2, scope, type, 1, 0, NULL);
                   tree* idNode = maketreeWithVal(IDENTIFIER, index);
                   addChild(varDeclNode, idNode);
                   addChild(varDeclNode, maketreeWithVal(INTEGER, $4));
                   $$ = varDeclNode;
                 }
                | typeSpecifier ID SEMICLN
                 {
                    tree* varDeclNode = maketree(VARDECL);
                    addChild(varDeclNode, $1);
                    if(ST_lookup($2, scope) != -1)
                      printf("Error: %d: Variable name already used\n", yylineno);
                    int index = ST_insert($2, scope, varDeclNode->children[0]->val, 0, 0, NULL);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(varDeclNode, idNode);
                    $$ = varDeclNode;
                 }
                ;

typeSpecifier   : KWD_INT
                 {
                    $$ = maketreeWithVal(TYPESPEC, INT_TYPE);
                 }
                | KWD_CHAR
                 {
                    $$ = maketree(CHAR_TYPE);
                 }
                | KWD_VOID
                 {
                    $$ = maketreeWithVal(TYPESPEC, VOID_TYPE);
                 }
                ;

funDecl         : typeSpecifier ID LPAREN 
		 { 
		   scope = $2; 
		 } 
		  formalDeclList RPAREN funBody 
                 {
                    int type = 0;
                    funDeclNode = maketree(FUNDECL);
                    addChild(funDeclNode, $1);
                    switch(funDeclNode->children[0]->nodeKind) {
                    case INT_TYPE:
                      type = INT_FUNC;
                      break;
                    case CHAR_TYPE:
                      type = CHAR_FUNC;
                      break;
                    case VOID_TYPE:
                      type = VOID_FUNC;
                      break;
                    default:
                      type = 0;
            }
                    if(ST_lookup($2, "global") != -1)
                      printf("Error: %d: Function name already used\n", yylineno);
                    int index = ST_insert($2, "global", type, 2, 0, NULL);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(funDeclNode, idNode);
                    addChild(funDeclNode, $5);
                    if ( $7 != NULL)
		      addChild(funDeclNode, $7);
                    
                    strTable[index]->size = numArgs;
                    numArgs = 0;
                    $$ = funDeclNode;
                 }
                | typeSpecifier ID LPAREN 
		 { 
		   scope = $2; 
		 } 
		   RPAREN funBody
                 {
                    strcpy(scopeName,$2);
                    funDeclNode = maketree(FUNDECL);
                    int type = 0;
                    addChild(funDeclNode, $1);
                    switch(funDeclNode->children[0]->nodeKind) {
                    case INT_TYPE:
                      type = INT_FUNC;
                      break;
                    case CHAR_TYPE:
                      type = CHAR_FUNC;
                      break;
                    case VOID_TYPE:
                      type = VOID_FUNC;
                      break;
                    default:
                      type = 0;
                    }
                    if(ST_lookup($2, "global") != -1)
                      printf("Error: %d: Function name already used\n", yylineno);
                    else {
		      int index = ST_insert($2, "global", type, 2, 0, NULL);
                      tree* idNode = maketreeWithVal(IDENTIFIER, index);
                      addChild(funDeclNode, idNode);
                    }
                    if ( $6 != NULL)
		      addChild(funDeclNode, $6);
		    $$ = funDeclNode;
                 } 
                ;

formalDeclList  : formalDecl
                  {
                    $$ = maketree(FORMALDECLLIST); 
                    addChild($$,$1);
                  }
                | formalDeclList COMMA formalDecl
                  {
                    $$ = $1;
                    addChild($$, $3);
                  }
                ;

formalDecl      : typeSpecifier ID
                 { 
                    tree* formalDeclNode = maketree(FORMALDECL);
                    addChild(formalDeclNode, $1);
                    int index = ST_insert($2, scope, 0, 0, 0, NULL);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(formalDeclNode, idNode);
                    $$ = formalDeclNode;
                    numArgs++;
                 }
                | typeSpecifier ID LSQ_BRKT RSQ_BRKT
                 {
                    int type = 0;
                    if (ST_lookup($2, scope) != -1)
                      printf("Error: %d: Variable name already used\n", yylineno);
                    tree* formalDeclNode = maketree(FORMALDECL);
                    addChild(formalDeclNode, $1);
            
                   switch(formalDeclNode->children[0]->nodeKind) {
                   case INT_TYPE:
                      type = INT_ARR;
                      break;
                   case CHAR_TYPE:
                      type = CHAR_ARR;
                      break;
                   case VOID_TYPE:
                      type = VOID_ARR;
                      break;
                   default:
                      type = 0;
            }
            
                    int index = ST_insert($2, scope, type, 1, 0, NULL);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(formalDeclNode, idNode);
                    $$ = formalDeclNode;
                 }
                ;

localDeclList   : varDecl
                 {
                    $$ = maketree(LOCALDECLLIST);
                    addChild($$, $1); 
                 }
                | localDeclList varDecl
                 {
                    $$ = $1;
                    addChild($$, $2);
                 }
                ;

statement       : compoundStmt { $$ = $1; }
                | assignStmt { $$ = $1; }
                | condStmt { $$ = $1; }
                | loopStmt { $$ = $1; }
                | returnStmt { $$ = $1; }
                ;

statementList   :  statement
                 {
                    $$ = maketree(STATEMENTLIST);
		    addChild($$, $1);
                 }
                |  statementList statement
                 {
                    $$ = $1;
		    addChild($$, $2);
                 }
                ;

funBody         :  LCRLY_BRKT statementList RCRLY_BRKT
                 {
                    tree* funBodyNode = maketree(FUNBODY);
                    addChild(funBodyNode, $2);
                    $$ = funBodyNode;
                 }
                |  LCRLY_BRKT localDeclList RCRLY_BRKT
                 {
                    tree* funBodyNode = maketree(FUNBODY);
                    addChild(funBodyNode, $2);
                    $$ = funBodyNode;
		 }
                |  LCRLY_BRKT localDeclList statementList RCRLY_BRKT
                 {
                    tree* funBodyNode = maketree(FUNBODY);
                    addChild(funBodyNode, $2);
                    addChild(funBodyNode, $3);
                    $$ = funBodyNode;
                 }
                |  LCRLY_BRKT RCRLY_BRKT
                 {
                    $$ = NULL;
		 }
                ;


compoundStmt    : LCRLY_BRKT statementList RCRLY_BRKT { $$ = $2; }
                ;

assignStmt      : var OPER_ASGN expression SEMICLN 
                 {
                    tree* assignStmtNode = maketree(ASSIGNSTMT);
                    addChild(assignStmtNode, $1);
                    addChild(assignStmtNode, $3);
/*                    
                    tree *tempVar = assignStmtNode->children[0];
                    while (tempVar->numChildren > 0)
                    tempVar = tempVar->children[0];
                    tree *tempExpr = assignStmtNode->children[1];
                    while (tempExpr->numChildren > 0)
                    tempExpr = tempExpr->children[0];

                    int varType = strTable[tempVar->val]->data_type;
                    int expType = -1;
                    if (tempExpr->nodeKind == IDENTIFIER) {
                       expType = strTable[tempExpr->val]->data_type;
                    } else {
                       expType = tempExpr->nodeKind;
                    }
                    if (!((varType == INT_TYPE) && (expType == INTEGER || expType == INTCONST)) &&
                    !((varType == VOID_TYPE) && (expType == VOID_TYPE)) &&
                    !((varType == CHAR_TYPE) && (expType == CHAR ||  expType == CHARCONST)))
                      printf("Error: %d: Type mismatch in assignment\n", yylineno);
*/
                    $$ = assignStmtNode;
                         }
                | expression SEMICLN { $$ = $1; }
                ;

condStmt        : KWD_IF LPAREN expression RPAREN statement
                 {
                    tree* condStmtNode = maketree(CONDSTMT);
                    addChild(condStmtNode, $3);
                    addChild(condStmtNode, $5);
                    $$ = condStmtNode;
                 }
                | KWD_IF LPAREN expression RPAREN statement KWD_ELSE statement
                 {
                    tree* condStmtNode = maketree(CONDSTMT);
                    addChild(condStmtNode, $3);
                    addChild(condStmtNode, $5);
                    addChild(condStmtNode, $7);
                    $$ = condStmtNode;
                 }
                ;

loopStmt        : KWD_WHILE LPAREN expression RPAREN statement
                 {
                    tree* loopStmtNode = maketree(LOOPSTMT);
                    addChild(loopStmtNode, $3);
                    addChild(loopStmtNode, $5);
                    $$ = loopStmtNode;
                 }
                ;

returnStmt      : KWD_RETURN SEMICLN
                 {
                    tree* returnStmtNode = maketree(RETURNSTMT);
		    $$ = returnStmtNode;
                 }
                | KWD_RETURN expression SEMICLN
                 {
                    tree* returnStmtNode = maketree(RETURNSTMT);
                    addChild(returnStmtNode, $2);
		    $$ = returnStmtNode;
                 }
                ;

var             : ID
                 {
                   int index = ST_lookup($1, scope);
                   tree* varNode = maketree(VAR);
                   if (index == -1)
                      printf("Error: %d: Undeclared Variable\n", yylineno);
                   tree* idNode = maketreeWithVal(IDENTIFIER, index);
                   addChild(varNode, idNode);
                   $$ = varNode;
                 }
                | ID LSQ_BRKT addExpr RSQ_BRKT
                 {  
                   tree* varNode = maketree(VAR);
                   int index = ST_lookup($1, scope);
                   tree* idNode = maketreeWithVal(IDENTIFIER, index);
                   addChild(varNode, idNode);
                   addChild(varNode, $3);
            
                    if (index == -1){
                      printf("Error: %d: Undeclared array\n", yylineno);
                    }
                    else if (strTable[index]->symbol_type != ARRAY){
                       printf("Error: %d: Non-array identifier used as an array\n", yylineno);
                    } else {
                      tree* temp = varNode->children[1];
                      while (temp->numChildren > 0) {
                      temp = temp->children[0];
                    } 
                      if (temp->nodeKind == IDENTIFIER){
                        if (strTable[temp->val]->data_type != INT_TYPE){ 
                            printf("Error: %d: Array indexed using non-integer expression\n", yylineno);
                        }
                      }
                      else {
                        if (temp->nodeKind != INTCONST)
                            printf("Error: %d: Array indexed using non-integer expression\n", yylineno);
                    
                        else {
                        int arrIndex = atoi(yyval.strval);
                        if (arrIndex >= strTable[index]->size)
                            printf("Error: %d: Indexing an array with an out-of-bounds integer\n", yylineno);
                        }
                    }
                 } 
                 $$ = varNode;
                 }
                ;

expression      : addExpr { $$ = $1; }
                | expression relop addExpr
                 {
		    addChild($2, $1);
		    addChild($2, $3);
		    $$ = $2;
                 }
                ;

relop           : OPER_LTE
                 {
                    
                    $$ = maketreeWithVal(RELOP, LTE);
                 }
                | OPER_LT 
                 {
                    $$ = maketreeWithVal(RELOP, LT);
                 }
                | OPER_GT 
                 {
                    $$ = maketreeWithVal(RELOP, GT);
                 }
                | OPER_GTE 
                 {
                    $$ = maketreeWithVal(RELOP, GTE);
                 }
                | OPER_EQ 
                 {
                    $$ = maketreeWithVal(RELOP, EQ);
                 }
                | OPER_NEQ 
                 {
                    $$ = maketreeWithVal(RELOP, NEQ);
                 }
                ;

addExpr         : term { $$ = $1; }
                | addExpr addop term
                 {
		    addChild($2, $1);
		    addChild($2, $3);
		    $$ = $2;
                 }
                ;

addop           : OPER_ADD
                 {
                    $$ = maketreeWithVal(ADDOP, ADD);
                 }
                | OPER_SUB
                 {
                    $$ = maketreeWithVal(ADDOP, SUB);
                 }
                ;

term            : factor { $$ = $1; }
                | term mulop factor
                 {
		    addChild($2, $1);
		    addChild($2, $3);
		    $$ = $2;
                 }
                ;

mulop           : OPER_MUL
                 {
                    $$ = maketreeWithVal(MULOP, MUL);
                 }
                | OPER_DIV
                 {
                    $$ = maketreeWithVal(MULOP, DIV);
                 }
                ;

factor          : LPAREN expression RPAREN { $$ = $2; }
                | var { $$ = $1; }
                | funcCallExpr { $$ = $1; }
                | INTCONST
                 {
                    tree* factorNode = maketree(FACTOR);
                    addChild(factorNode, maketreeWithVal( INTEGER,  $1 ));
                    $$ = factorNode;
                 }
                | CHARCONST
                 {
                    tree* factorNode = maketree(FACTOR);
                    addChild(factorNode, maketreeWithVal( CHAR,  $1 ));
                    $$ = maketreeWithVal(FACTOR, $1);
                 }
                | STRCONST
                 {
                    $$ = maketreeWithVal(FACTOR, $1);
                 }
                ;
//ST_lookup
funcCallExpr    : ID LPAREN argList RPAREN 
                 {
                    int index = ST_lookup($1, "global");
                    if (index == -1)
                      printf("Error: %d: Undeclared Function\n", yylineno);
                    
                    else if (numArgs < strTable[index]->size)
                      printf("Error: %d: Too few arguments provided in function call\n", yylineno);
                    else if (numArgs > strTable[index]->size)
                      printf("Error: %d: Too many arguments provided in function call\n", yylineno);

                    tree* funcCallExprNode = maketree(FUNCCALLEXPR);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(funcCallExprNode, idNode);
                    addChild(funcCallExprNode, $3);
                    $$ = funcCallExprNode;
                    numArgs = 0;
                 }
                | ID LPAREN RPAREN
                 {
                   int index = ST_lookup($1, "global");
                   if (index == -1)
                      printf("Error: %d: Undeclared Function\n", yylineno);
                    tree* funcCallExprNode = maketree(FUNCCALLEXPR);
                    tree* idNode = maketreeWithVal(IDENTIFIER, index);
                    addChild(funcCallExprNode, idNode);
                    $$ = funcCallExprNode;
                 }
                ;

argList         : expression
                 {  numArgs++;
		    $$ = maketree(ARGLIST);
                    addChild($$, $1);
                 }
                | argList COMMA expression
                 {
                    numArgs++;
                    $$ = $1;
                    addChild($$, $3);
                 }
                ;
%%

int yywarning(char * msg){
    printf("warning: line %d: %s\n", yylineno, msg);
   return 0;
}

int yyerror(char * msg){
    printf("error: line %d: %s\n", yylineno, msg);
    return 0;
}
