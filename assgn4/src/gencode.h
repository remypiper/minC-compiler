#ifndef GENCODE_H
#define GENCODE_H

void emitAssignStatement(FILE* outfile, tree* node);
void emitCondStatement(FILE* outfile, tree* node);
void emitLoopStatement(FILE* outfile, tree* node);
void emitFunc(FILE* outfile, tree* node);
void gencode(FILE* outfile, tree* node, int nestLevel);
int expr(FILE* outfile, tree* node);
int nextReg();

#endif
