#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<../src/tree.h>
#include<../src/strtab.h>
#include<../src/gencode.h>

extern FILE* yyin;

void printhelp(){
    printf("Usage: mcc [--ast] [--sym] [--gen | --gen --outfile <FILE>] [-h|--help] FILE\n");
    printf("\t--ast:\t\tPrint a textual representation of the constructed abstract syntax tree.\n");
    printf("\t--sym:\t\tPrint a textual representation of the constructed symbol table.\n");
    printf("\t-h,--help:\tPrint this help information and exit.\n\n");
    printf("\t--gen:\t\tPrint generated MIPS assembly code to stdout.\n");
    printf("\t--gen --outfile <FILE>:\t\tPrint generated MIPS assembly code to FILE.\n");
    printf("\n\tExample: Print the ast, print the symbol table, and print generated MIPS assembly to the file ex.asm:\n\n\t\tmcc --ast --sym --gen --outfile ex.asm testAssign.mC\n\n");
}

int main(int argc, char *argv[]) {
    for (int i = 0; i < MAXIDS; i++) {
        strTable[i] = (struct strEntry*) malloc(sizeof(struct strEntry)); // Go ahead and allocate all 1000 entries. Could be better.
        strTable[i]->id = NULL;
        strTable[i]->scope = NULL;
        strTable[i]->data_type = -1;
        strTable[i]->symbol_type = -1;
    }
    int p_ast = 0;
    int p_symtab = 0;
    int p_gencode = 0;
    FILE* outfile = NULL;

    ST_insert("output", "global", 0, 2, 0, NULL);

    // Skip first arg (program name), then check all but last for options.
    for(int i=1; i < argc - 1; i++){
        if(strcmp(argv[i],"-h")==0 || strcmp(argv[i],"--help")==0){
            printhelp();
            return 0;
        }
        else if(strcmp(argv[i],"--ast")==0){
            p_ast = 1;
        }
        else if(strcmp(argv[i],"--sym")==0){
            p_symtab = 1;
        }
        else if(strcmp(argv[i],"--gen")==0){
            p_gencode = 1;
        }
        else if(strcmp(argv[i],"--outfile")==0){
            if (++i < argc) outfile = fopen(argv[i], "w"); // If it's the last arg.
                else{
                printhelp();
                return 0;
            }
        }
        else{
            printhelp();
            return 0;
        }

    }
    
    if (!outfile) outfile = stdout;

    yyin = fopen(argv[argc - 1],"r");
    if(!yyin){
        printf("error: unable to read source file %s\n",argv[argc-1]);
        return -1;
    }

    if (!yyparse()){
        printf("Compilation finished.\n\n");
        if (p_ast) {
            printAst(ast, 1);
        }
        if (p_symtab) {
            print_sym_tab();
        }
        if (p_gencode) {
            if (outfile == stdout) printf("ASM:\n");
            // Top of every asm file has these sections.
            fprintf(outfile, "\t.data\n");
            fprintf(outfile, "\t.text\n");
            fprintf(outfile, "\t.globl main\n");
            gencode(outfile, ast, 1);
        }
    }
    return 0;
}
