1. My name is Remy Piper, my partner is Garrett Faber. My course id is CS4318, and Garrett's is CS5331.

2. We plan to meet as often as we can. We don't meet virtually, except throught discord chat. We meet in person in the library.

3. We both implemented a substantial part of the assembly output. Garrett revised the grammar by collapsing the tree, and I did work on outputting conditional statements.

4. We created multilple test cases to test our code, which are included in the root directory of this assignment (assgn4/).

5. Our output(int) function does not correctly generate into assembly code that outputs the specified integer. Currently it outputs 0 everytime. Also, we don't have variables initialized in the .data segment of the generated assembly, meaning we currently generate code with labels that the assembler cannot recognize.

6. Our calling convention is the same as the normal MIPS implementation:

Number  | Name      | Description
0       | $zero     | The value 0
2-3     | $v0 - $v1 | (values) from expression evaluation and function results
4-7     | $a0 - $a3 | (arguments) First four parameters for subroutine
8-15    | $t0 - $t7 | Temporary variables
16-23   | $s0 - $s7 | Saved values representing final computed results
24-25   | $t8 - $t9 | Temporary variables2
31      | $ra       |Return address

7. Please use our main branch, and not the testing branch.

Compile the code with:

    make clean; make

And run the code wth:

    ./mcc --ast --sym --gen --outfile <outfile.asm> <infile.mC>

For example, the following command prints the ast, prints the symbol table, and print generated MIPS assembly to the file ex.asm, taking the file testAssign.mC as input:

    mcc --ast --sym --gen --outfile ex.asm testAssign.mC

More documentation can be found in the printhelp() function from our driver.c file.
